const data = require('../service/fetch-data');
const pg = require('pg');
const config = require('config');
const postgresConf = config.get('postgres');
const pool = new pg.Pool(postgresConf);
const myFunc = require('../public/libs/process');
const pay = require('../contant/pay.contant');
const maximumSaved = require('../contant/maximumSaved.contant');
const role = require('../contant/role.contant');
const bcrypt = require('bcrypt');

//console.log(role);

//var obj=[];

//console.log(obj);

const getIndexPage = (req, res, next) => {
	let roleSession;
	let ten;

	if(req.session.role==null)
	{
		roleSession="";
		ten = "Chưa đăng nhập";
	}
	else
	{
		roleSession=req.session.role;
		if(req.session.role == role.user && req.session.pay == pay.paid)
		{
			ten=req.session.ten + " - VIP";
		}
		else if(req.session.role == role.admin)
		{
			ten=req.session.ten + " - ADMIN";
		}
		else
		{
			ten=req.session.ten;
		}
	}
	//console.log(obj);

	res.render('index', {alert: '', alert1: '', alert2: '', roleSession, ten, alert3:''});
}

const register = (req, res, next) => {
	let alert="";
	let roleSession="";
	let {hoTen, sdt, matKhau, nhapLaiMatkhau, href} = req.body;

	if(hoTen=="" || sdt == "" || matKhau == "" || nhapLaiMatkhau == "")
	{
		alert="Thông tin chưa đầy đủ. Vui lòng kiểm tra lại thông tin!";
		res.render(href, {alert, alert1:'', alert2: '', roleSession, ten:'Chưa đăng nhập', alert3:'' });
	}
	else
	{
		if(sdt.length != 10)
		{
			alert="Số điện thoại phải là 10 số! Vui lòng kiểm tra lại.";
			res.render(href, {alert, alert1:'', alert2: '', roleSession, ten:'Chưa đăng nhập', alert3:''});
		}
		else
		{
			data('SELECT * FROM nguoidung WHERE sdt = $1', [sdt], (err, result) =>{
				if(err){
					res.end();
					return console.error("error running query", err);
				}
				if(result.rows[0]==null)
				{
					if(matKhau!=nhapLaiMatkhau)
					{
						alert="Mật khẩu nhập lại không khớp! Vui lòng kiểm tra lại.";
						res.render(href, {alert, alert1:'', alert2: '', roleSession, ten:'Chưa đăng nhập', alert3:''});
					}
					else
					{
						var salt = bcrypt.genSaltSync(10);
						var hash = bcrypt.hashSync(matKhau, salt);

						data('INSERT INTO nguoidung(ten, sdt, matkhau, quyen, thanhtoan, salt) VALUES($1,$2,$3,$4,$5,$6)',
						 [hoTen, sdt, hash, role.user, pay.unpaid, salt], (err, result) =>{
							if(err){
								alert="Đăng ký thất bại!";
								res.render(href, {alert, alert1:'', alert2: '', roleSession, ten:'Chưa đăng nhập', alert3:''});
								res.end();
								return console.error("error running query", err);
							}
							else
							{
								alert="Đăng ký thành công.";
								res.render(href, {alert, alert1:'', alert2: '', roleSession, ten:'Chưa đăng nhập', alert3:''});
							}	
						}); 
					}
				}
				else
				{
					alert="Số điện thoại đã được đăng ký! Vui lòng kiểm tra lại.";
					res.render(href, {alert, alert1:'', alert2: '', roleSession, ten:'Chưa đăng nhập', alert3:''});
				}				
			});
		}
	}
};

const changePassWord = (req, res, next) => {
	let alert2="";
	let roleSession=req.session.role;
	let phoneNumber=req.session.sdt;
	let ten=req.session.ten;

	let {matKhauCu, matKhauMoi, matKhauMoiNhapLai, href} = req.body;

	if(matKhauCu=="" || matKhauMoi == "" || matKhauMoiNhapLai == "")
	{
		alert2="Thông tin chưa đầy đủ. Vui lòng kiểm tra lại thông tin!";
		res.render(href, {alert:'', alert1:'', alert2, roleSession, ten, alert3:''});
	}
	else
	{
		if(matKhauMoi!=matKhauMoiNhapLai)
		{
			alert2="Mật khẩu mới nhập lại không khớp! Vui lòng kiểm tra lại.";
			res.render(href, {alert:'', alert1:'', alert2, roleSession, ten, alert3:''});
		}
		else
		{
			data('SELECT * FROM nguoidung WHERE sdt = $1', [req.session.sdt], (err, result) =>{
				if(err){
					res.end();
					return console.error("error running query", err);
				}
				let checkPW = bcrypt.compareSync(matKhauCu, result.rows[0].matkhau);

				if(checkPW==false)
				{
					alert2="Mật khẩu cũ không đúng! Vui lòng kiểm tra lại.";
					res.render(href, {alert:'', alert1:'', alert2, roleSession, ten, alert3:''});
				}
				else
				{
					var salt = bcrypt.genSaltSync(10);
					var hash = bcrypt.hashSync(matKhauMoi, salt);

					data('UPDATE nguoidung set matkhau=$1 WHERE sdt=$2', [hash, phoneNumber], (err, result1) =>{
						if(err)
						{
							res.end();
							return console.error("error running query", err);
						}
						alert2="Đổi mật khẩu thành công.";
						res.render(href, {alert:'', alert1:'', alert2, roleSession, ten, alert3:''});
					});
				}		
			});
		}
	}
}

const login =  (req, res, next) => {
	let alert="";
	let alert1="";
	let roleSession="";
	let ten ="Chưa đăng nhập";

	let {sdt, matKhau, href} = req.body;
	
	if(sdt=="" || matKhau=="")
	{
		alert1="Thông tin chưa đầy đủ. Vui lòng kiểm tra lại thông tin!";
		res.render(href, {alert, alert1, alert2: '', roleSession, ten, alert3:''});
	}
	else
	{
		if(sdt.length != 10)
		{
			alert1="Số điện thoại phải là 10 chữ số! Vui lòng kiểm tra lại.";
			res.render(href, {alert, alert1, alert2: '', roleSession, ten, alert3:''});
		}
		else
		{
			data('SELECT * FROM nguoidung WHERE sdt = $1', [sdt], (err, result) =>{
				if(err){
					res.end();
					return console.error("error running query", err);
				}
				if(result.rows[0]==null)
				{
					alert1="Số điện thoại hoặc mật khẩu không đúng! Vui lòng kiểm tra lại.";
				}
				else
				{
					//console.log(result.rows[0]);
					let checkPW = bcrypt.compareSync(matKhau, result.rows[0].matkhau);
					if(checkPW == false)
					{
						alert1="Số điện thoại hoặc mật khẩu không đúng! Vui lòng kiểm tra lại.";
					}
					else
					{
						req.session.sdt = result.rows[0].sdt;
						req.session.ten = result.rows[0].ten;
						//console.log(req.session.sdt);
						req.session.role = result.rows[0].quyen;
						req.session.pay = result.rows[0].thanhtoan;
						roleSession=result.rows[0].quyen;

						if(result.rows[0].quyen == role.user && result.rows[0].thanhtoan == pay.paid)
						{
							ten=req.session.ten + " - VIP";
						}
						else if(result.rows[0].quyen == role.admin)
						{
							ten=req.session.ten + " - ADMIN";
						}
						else
						{
							ten=req.session.ten;
						}

						alert1="Đăng nhập thành công.";
					}
				}
				res.render(href, {alert, alert1, alert2: '', roleSession, ten, alert3:''});				
			});
		}
	}
	
};

const logout= (req, res, next) => {
	delete req.session.sdt;
	delete req.session.role;
	delete req.session.pay;
	delete req.session.ten
  	res.redirect('/');
};

//lấy trang bản thân
const getBanThanPage = (req, res, next) => {
  res.render("nhap-thong-tin-ca-nhan");
};

//lấy trang tứ trụ
const getTuTruPage =  (req, res, next) => {
  res.render("tu-tru");
};

//lấy giờ hoàng đạo
const getTime = (req, res, next) => {
    let timeData=[];
	let month=Number(req.body.month);

	data('SELECT * FROM giohoangdao WHERE thang = $1',[month], (err, result) =>{
		if(err) 
		{
			res.end();
			return console.error("error running query", err);
		}
		//console.log(result.rows);
		for(let i=0 ; i<12; i++)
		{
			timeData.push(result.rows[i]);
		}
		res.send(timeData);
	});
};

//lấy trang bắt dầu xem
const postBatDauXemPage=(req, res, next) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;

	var alert = ''
	var obj1 = req.body.demo;
	var obj = req.session.obj;

	obj1 = JSON.parse(req.body.demo);

	if(req.session.sdt == null)
	{
		if(obj1.namduong != 0)
		{
			if(obj == null)
			{

				req.session.obj = [];
				req.session.obj.push(obj1);
			}
			else
			{
				console.log(obj.length);	
				if(obj.length >= 2)
				{
					alert="Bạn chỉ được lưu một thông tin nam và một thông tin nữ. Hãy đăng kí thành viên để được lưu nhiều hơn."
				}
				else
				{
					if(obj[0].gioitinh == "Nam" && obj1.gioitinh== "Nam" || obj[0].gioitinh == "Nữ" && obj1.gioitinh== "Nữ")
					{
						alert="Bạn chỉ được lưu một thông tin nam và một thông tin nữ. Hãy đăng kí thành viên để được lưu nhiều hơn."
					}
					else
					{
						req.session.obj.push(obj1);
					}	
				}		
			}
		}
	}
	else
	{
		if(obj1.namduong != 0)
		{
			if(obj == null)
			{

				req.session.obj = [];
				req.session.obj.push(obj1);
			}
			else
			{
				obj.forEach(function(val, item){
					if(val.gioitinh == "Nam" && obj1.gioitinh== "Nam" || val.gioitinh == "Nữ" && obj1.gioitinh== "Nữ")
					{
						req.session.obj.splice(item,1,obj1);
						//req.session.obj.push(obj1);
					}
					else
					{
						req.session.obj.push(obj1);
					}			
				});
			}
		}
	}
	console.log(req.session.obj);

	let arr = myFunc.getYear(sessionRole, sessionPay, role, pay);
	//console.log(arr[0]);

	res.render('bat-dau-xem', {arr, alert});
}

const getBatDauXemPage=(req, res, next) => {

	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;

	let arr = myFunc.getYear(sessionRole, sessionPay, role, pay);
	//console.log(arr[0]);

	res.render('bat-dau-xem', {arr, alert: ''});
}

//lấy thông tin toàn bộ bên nam
const getMaleInfo = (req, res, next) => {
	data('SELECT * FROM khachhang WHERE gioitinh = $1 AND userid = $2', ["Nam", req.session.sdt], (err, result) =>{
		if(err)
		{
			res.end();
			return console.error("error running query", err);
		}
		//console.log(req.session.sdt);
		//console.log(result.rows);
		res.send(result.rows);
	});
};


//lấy thong 6tin toàn bên nữ
const getFemaleInfo = (req, res, next) => {
	data('SELECT * FROM khachhang WHERE gioitinh = $1 AND userid = $2', ["Nữ", req.session.sdt], (err, result) =>{
		if(err)
		{
			res.end();
			return console.error("error running query", err);
		}
		//console.log(result.rows);
		res.send(result.rows);
	});
};

//lấy dữ liệu khi bấm nút submit
const getSubmittedData = (req, res, next) => {
	if(req.session.obj==null)
	{
		res.send("0");
	}
	else{
		res.send(req.session.obj);
	}
	
};

//lấy hướng phong thủy
const getFengShuiDirection = (req, res, next) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	if(sessionRole==role.user && sessionPay==pay.unpaid || sessionPhoneNumber==null)
	{
		res.send("0");
	}
	else
	{
		let {cungPhi}=req.body;

		data('SELECT * FROM huongphongthuy WHERE cungphi = $1', [cungPhi], (err, result) =>{
			if(err)
			{
				res.end();
				return console.error("error running query", err);
			}
			res.send(result.rows[0]);
		});
	}
};


//lấy kết hợp cung sanh
const getCombination = (req, res, next) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	if(sessionRole==role.user && sessionPay==pay.unpaid || sessionPhoneNumber==null)
	{
		res.send("0");
	}
	else
	{
		let{cungPhiNam, cungPhiNu}=req.body;

		data('SELECT * FROM kethop WHERE cungphinam = $1 AND  cungphinu = $2', [cungPhiNam, cungPhiNu], (err, result) =>{
			if(err)
			{
				res.end();
				return console.error("error running query", err);
			}
			res.send(result.rows[0]);
		});
	}
};

//lưu thông tin khách hàng
const saveInfo  = (req, res, next) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	if(sessionPhoneNumber==null)
	{
		res.send("Hãy đăng nhập để lưu thông tin");
	}
	else
	{
		data('SELECT COUNT(userid) FROM khachhang WHERE userid = $1', [sessionPhoneNumber], (err, result) =>{
			if(err)
			{
				res.end();
				return console.error("error running query", err);
			}
			if(sessionRole==role.user && sessionPay==pay.unpaid && result.rows[0].count>=maximumSaved.normal)
			{
				res.send("Bạn sử dụng hết số lần lưu thông tin. Hãy nâng cấp Tài khoản VIP để lưu trữ nhiều hơn");
			}
			else
			{

				let ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong, ngayAm, thangAm, namAm, canAm, chiAm, conThu, 
				namSinhCha, namSinhMe, namSinhDoiPhuong, namSinhMeDoiPhuong, namSinhChaDoiPhuong, soCon, namSinhCacCon, namKetHon, conThuDP, gioiTinhCacCon;

				ten=req.body.ten;
				gioiTinh=req.body.gioiTinh;
				gio=req.body.gio;
				ngayDuong=Number(req.body.ngayDuong);
				thangDuong=Number(req.body.thangDuong);
				namDuong=Number(req.body.namDuong);
				ngayAm=Number(req.body.ngayAm);
				thangAm=Number(req.body.thangAm);
				namAm=Number(req.body.namAm);
				canAm=req.body.canAm;
				chiAm=req.body.chiAm;
				conThu=Number(req.body.conThu);
				namSinhCha=Number(req.body.namSinhCha);
				namSinhMe=Number(req.body.namSinhMe);
				namSinhDoiPhuong=Number(req.body.namSinhDoiPhuong);
				namSinhChaDoiPhuong=Number(req.body.namSinhChaDoiPhuong);
				namSinhMeDoiPhuong=Number(req.body.namSinhMeDoiPhuong);
				soCon=Number(req.body.soCon);
				namSinhCacCon=req.body.namSinhCacCon;
				namKetHon=Number(req.body.namKetHon);
				conThuDP=Number(req.body.conThuDP);
				gioiTinhCacCon=req.body.gioiTinhCacCon;
				let date = new Date();

				if(sessionRole==role.user && sessionPay==pay.unpaid)
				{
					data('SELECT * FROM khachhang WHERE userid = $1', [sessionPhoneNumber], (err, result) =>{
						if(err)
						{
							res.end();
							return console.error("error running query", err);
						}
						else
						{
							if(result.rows.length > 1)
							{
								var nam = 0;
								var nu = 0;

								if(gioiTinh=="Nam")
								{
									nam++;
								}
								else
								{
									nu++;
								}

								result.rows.forEach(function(val){
									if(val.gioitinh == "Nam")
									{
										nam++;
									}
									else
									{
										nu++;
									}
								});

								if(nam > 2 || nu > 2 )
								{
									res.send("Tài khoản Thường chỉ được lưu hai Nam và hai Nữ.");
								}
								else
								{
									data('INSERT INTO khachhang(ten, gioitinh, giohoangdao, ngayduong, thangduong, namduong, ngayam, thangam, namam, canam, chiam, conthu, namsinhcha, namsinhme, namsinhdoiphuong, namsinhchadoiphuong, namsinhmedoiphuong, socon, namsinhcaccon, namkethon, conthudp, gioitinhcaccon, userid, created_at, updated_at) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22, $23, $24, $25)',
									 [ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong, ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe, namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon, namKetHon, conThuDP, gioiTinhCacCon, sessionPhoneNumber, date, date], (err, result) =>{
										if(err){
											res.send("Thêm thất bại")
											res.end();
											return console.error("error running query", err);
										}
										res.send("Thêm thành công");
									}); 
								}
							}
							else
							{
								data('INSERT INTO khachhang(ten, gioitinh, giohoangdao, ngayduong, thangduong, namduong, ngayam, thangam, namam, canam, chiam, conthu, namsinhcha, namsinhme, namsinhdoiphuong, namsinhchadoiphuong, namsinhmedoiphuong, socon, namsinhcaccon, namkethon, conthudp, gioitinhcaccon, userid, created_at, updated_at) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22, $23, $24, $25)',
								 [ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong, ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe, namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon, namKetHon, conThuDP, gioiTinhCacCon, sessionPhoneNumber, date, date], (err, result) =>{
									if(err){
										res.send("Thêm thất bại")
										res.end();
										return console.error("error running query", err);
									}
									res.send("Thêm thành công");
								}); 
							}	
						}
					});
				}	
				else
				{
					data('INSERT INTO khachhang(ten, gioitinh, giohoangdao, ngayduong, thangduong, namduong, ngayam, thangam, namam, canam, chiam, conthu, namsinhcha, namsinhme, namsinhdoiphuong, namsinhchadoiphuong, namsinhmedoiphuong, socon, namsinhcaccon, namkethon, conthudp, gioitinhcaccon, userid, created_at, updated_at) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22, $23, $24, $25)',
					 [ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong, ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe, namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon, namKetHon, conThuDP, gioiTinhCacCon, sessionPhoneNumber, date, date], (err, result) =>{
						if(err){
							res.send("Thêm thất bại")
							res.end();
							return console.error("error running query", err);
						}
						res.send("Thêm thành công");
					}); 
				}
			}
		});
	}
};

//lấy thông tin cở bản
const getDetail = (req, res, next) => {
	let year=Number(req.body.year)%60;

	data('SELECT * FROM saumuoinam WHERE id = $1', [year], (err, result) =>{
		if(err)
		{
			res.end();
			return console.error("error running query", err);
		}
		res.send(result.rows[0]);
	});
};

//lấy thông tin vận niên
const getPeriodOfYear = (req, res, next) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	let tuoi=Number(req.body.tuoi);
	
	data('SELECT * FROM vannien WHERE id = $1', [tuoi], (err, result) =>{
		if(err)
		{
			res.end();
			return console.error("error running query", err);
		}
		else
		{
			if(sessionPhoneNumber == null || sessionRole==role.user && sessionPay==pay.unpaid)
			{
				result.rows[0].gtsaonam="0";
				result.rows[0].gtsaonu="0";
				result.rows[0].gthannam="0";
				result.rows[0].gthannu="0";
				result.rows[0].gtvannien="0";
				result.rows[0].gthoangoc="0";
				if(result.rows[0].gtkimlau!=null)
				{
					result.rows[0].gtkimlau="0";	
				}	
				//console.log(result.rows[0]);
				res.send(result.rows[0]);
			}
			else
			{
				//console.log(result.rows[0]);
				res.send(result.rows[0]);
			}		
		}	
	});
};

//lấy thái tuế
const getTaiSui = (req, res, next) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

 	let {tuoi, namHienTai} = req.body;
 	let taiSuiData;

 	data('SELECT * FROM thaitue WHERE tuoi = $1 AND namhientai = $2', [tuoi, namHienTai], (err, result) =>{
		if(err){
			res.end();
			return console.error("error running query", err);
		}
		if(result.rows[0]==null)
		{
			taiSuiData="0";
			res.send(taiSuiData);
		}
		else
		{
			if(sessionPhoneNumber == null || sessionRole==role.user && sessionPay==pay.unpaid)
			{
				result.rows[0].gtxungthaitue="0";
				
				if(result.rows[0].hthaitue!=null)
				{
					result.rows[0].gthinhthaitue="0";
				}

				taiSuiData=result.rows[0];
				//console.log(result.rows[0]);
				res.send(taiSuiData);
			}
			else
			{
				taiSuiData=result.rows[0];
				//console.log(result.rows[0]);
				res.send(taiSuiData);
			}	
		}
	});
};

//lấy thông tin tam tai
const getThreeFatalYears = (req, res, next) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

 	let {tuoi, namHienTai}= req.body;
 	let threeFatalYearsData;

 	data('SELECT * FROM tamtai WHERE tuoi = $1 AND namhientai = $2', [tuoi, namHienTai], (err, result) =>{
 		if(err){
			res.end();
			return console.error("error running query", err);
		}
		if(result.rows[0]==null)
		{
			threeFatalYearsData="0";
			res.send(threeFatalYearsData);
		}
		else
		{
			if(sessionPhoneNumber == null || sessionRole==role.user && sessionPay==pay.unpaid)
			{
				result.rows[0].gttamtai="0";

				threeFatalYearsData=result.rows[0];
				//console.log(result.rows[0]);
				res.send(threeFatalYearsData);
			}
			else
			{
				threeFatalYearsData=result.rows[0];
				//console.log(result.rows[0]);
				res.send(threeFatalYearsData);
			}	
		}
	});
};

//lấy thông tin không cưới hỏi
const getNotMarried = (req, res, next) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

 	let {gioiTinh , tuoi, namHienTai} = req.body;
 	let notMarriedData;

 	data('SELECT * FROM kocuoihoi WHERE gioitinh=$1 AND tuoi = $2 AND namhientai = $3', [gioiTinh ,tuoi, namHienTai], (err, result) =>{
 		if(err){
			res.end();
			return console.error("error running query", err);
		}
		if(result.rows[0]==null)
		{
			notMarriedData="0";
			res.send(notMarriedData);
		}
		else
		{
			if(sessionPhoneNumber == null || sessionRole==role.user && sessionPay==pay.unpaid)
			{
				result.rows[0].gtcuoihoi="0";

				notMarriedData=result.rows[0];
				res.send(notMarriedData);
			}
			else
			{
				notMarriedData=result.rows[0];
				res.send(notMarriedData);
			}
		}
	});
};

const getTimes=(req, res, next) =>
{
	let {canThanChu, canNamHienTai} = req.body;
 	let timesData;

 	data('SELECT * FROM thoithe WHERE canthanchu = $1 AND canhientai = $2', [canThanChu, canNamHienTai], (err, result) =>{
		if(err){
			res.end();
			return console.error("error running query", err);
		}
		if(result.rows[0]==null)
		{
			timesData="0";
			res.send(timesData);
		}
		else
		{
			res.send(result.rows[0]);	
		}
	});
};

const getWuXing=(req, res, next)=>{
	let {nguHanh, thang} = req.body;

	data('SELECT * FROM tsds WHERE mang = $1 AND thang = $2', [nguHanh, thang], (err, result) =>{
	if(err){
		res.end();
		return console.error("error running query", err);
	}
	else
	{
		res.send(result.rows[0]);
	}
	});
};

module.exports = {
  getIndexPage,
  getTime,
  getDetail,
  getBanThanPage,
  getTuTruPage,
  saveInfo,
  getBatDauXemPage,
  getMaleInfo,
  getFemaleInfo,
  getPeriodOfYear,
  getTaiSui,
  getThreeFatalYears,
  getNotMarried,
  getFengShuiDirection,
  getCombination,
  getSubmittedData,
  register,
  login,
  logout,
  changePassWord,
  postBatDauXemPage,
  getWuXing,
  getTimes
};