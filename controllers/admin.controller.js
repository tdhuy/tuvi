const data = require('../service/fetch-data');
const pg = require('pg');
const config = require('config');
const postgresConf = config.get('postgres');
const pool = new pg.Pool(postgresConf);
const myFunc = require('../public/libs/process');
const pay = require('../contant/pay.contant');
const maximumSaved = require('../contant/maximumSaved.contant');
const role = require('../contant/role.contant');
const defaultPW = require('../contant/defaultPassword.contant');
const bcrypt = require('bcrypt');

const getIndexPage = (req, res, next) => {
  	res.render('quan-ly-thanh-vien');
};


const getRoleDataUsers=(req, res, next ) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	if(sessionPhoneNumber==null || sessionRole==role.user)
	{
		res.redirect("/");
	}
	else
	{
		data('SELECT ten, sdt, quyen, thanhtoan FROM nguoidung WHERE quyen=$1 AND thanhtoan = $2', [role.user, pay.unpaid], (err, result1) =>{
			if(err)
			{
				res.end();
				return console.error("error running query", err);
			}
			//console.log(result1.rows);
			res.send(result1.rows);
		});
	}
};

const getDataUsers=(req, res, next ) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	if(sessionPhoneNumber==null || sessionRole==role.user)
	{
		res.redirect("/");
	}
	else
	{
		data('SELECT ten, sdt, quyen, thanhtoan FROM nguoidung WHERE quyen=$1', [role.user], (err, result1) =>{
			if(err)
			{
				res.end();
				return console.error("error running query", err);
			}
			//console.log(result1.rows);
			res.send(result1.rows);
		});
	}
};

const updateRoleUser=(req, res, next) =>{
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	let {sdt} = req.body;

	if(sessionPhoneNumber==null || sessionRole==role.user)
	{
		res.redirect("/");
	}
	else
	{
		data('UPDATE nguoidung SET thanhtoan = $1 WHERE sdt= $2', [pay.paid, sdt], (err, result1) =>{
			if(err)
			{
				res.send("Cấp quyền thất bại");
				res.end();
				return console.error("error running query", err);
			}
			//console.log(result1.rows);
			res.send("Cấp quyền thành công");
		});
	}
};

const forgotPassword=(req, res, next) =>{
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	let {sdt} = req.body;

	if(sessionPhoneNumber==null || sessionRole==role.user)
	{
		res.redirect("/");
	}
	else
	{
		var salt = bcrypt.genSaltSync(10);
		var hash = bcrypt.hashSync(defaultPW.defaultPassword, salt);

		data('UPDATE nguoidung SET matkhau = $1 WHERE sdt= $2', [hash, sdt], (err, result1) =>{
			if(err)
			{
				res.send("Khôi phục mật khẩu thất bại");
				res.end();
				return console.error("error running query", err);
			}
			//console.log(result1.rows);
			res.send("Khôi phục mật khẩu thành công");
		});
	}
};

const getContent =  (req, res, next) =>{
	if(req.session.sdt == null || req.session.role==role.user)
	{
		res.redirect("/");
	}
	else
	{
		data('SELECT * FROM lienhe WHERE dalienhe = $1',[1], (err, result) =>{
			if(err){
				res.end();
				return console.error("error running query", err);
			}
			res.send(result.rows);
		}); 
	}
};

const updateContent =  (req, res, next) =>{
	if(req.session.sdt == null || req.session.role==role.user)
	{
		res.redirect("/");
	}
	else
	{
		let{id}= req.body;

		data('UPDATE lienhe set dalienhe = $1 WHERE id = $2 ',[2, id], (err, result) =>{
			if(err){
				res.end();
				return console.error("error running query", err);
			}
			res.send("Đã liên hệ");
		}); 
	}
};

const getLienHePage = (req, res, next) =>{
	if(req.session.sdt == null || req.session.role==role.user)
	{
		res.redirect("/");
	}
	else
	{
		res.render("thong-tin-tu-van");
	}
};

module.exports = {
  getIndexPage,
  getDataUsers,
  getRoleDataUsers,
  forgotPassword,
  updateRoleUser,
  updateContent,
  getContent,
  getLienHePage
};