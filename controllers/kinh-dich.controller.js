const data = require('../service/fetch-data');
const pg = require('pg');
const config = require('config');
const postgresConf = config.get('postgres');
const pool = new pg.Pool(postgresConf);
const myFunc = require('../public/libs/process');
const pay = require('../contant/pay.contant');
const maximumSaved = require('../contant/maximumSaved.contant');
const role = require('../contant/role.contant');
const bcrypt = require('bcrypt');

const postKinhDichPage=(req, res, next)=>{
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;

	if(req.session.sdt == null)
	{
		res.render('index', {alert: '', alert1: '', alert2: '', roleSession: '', ten: 'Chưa đăng nhập', alert3:"Hãy nâng cấp lên tài khoản VIP để được xem"});
	}
	else if(sessionRole == role.user && sessionPay== pay.unpaid)
	{
		res.render('index', {alert: '', alert1: '', alert2: '', roleSession: sessionRole, ten: req.session.ten, alert3:"Hãy nâng cấp lên tài khoản VIP để được xem"});
	}
	else
	{
		let arr = myFunc.getYearForYiJing();

		res.render("kinh-dich", {arr});
	}
};

const getKinhDichPage=(req, res, next) =>
{
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;

	if(req.session.sdt == null || sessionRole == role.user && sessionPay== pay.unpaid)
	{
		res.redirect("/");
	}
	else
	{
		let arr = myFunc.getYearForYiJing();

		res.render("kinh-dich", {arr});
	}
};

const getUsers = (req, res, next) =>
{
	if(req.session.sdt == null)
	{
		res.redirect("/");
	}
	else
	{
		data('SELECT * FROM khachhang WHERE userid = $1', [req.session.sdt], (err, result) =>{
			if(err)
			{
				res.end();
				return console.error("error running query", err);
			}
			res.send(result.rows);
		});
	}
};

const getYiJing = (req, res, next) =>
{
	if(req.session.sdt == null)
	{
		res.redirect("/");
	}
	else
	{
		let {que} = req.body;
		data('SELECT * FROM kinhdich WHERE que = $1', [que], (err, result) =>{
			if(err)
			{
				res.end();
				return console.error("error running query", err);
			}
			res.send(result.rows[0]);
		});
	}
};

module.exports = {
	getKinhDichPage,
	getUsers,
	getYiJing,
	postKinhDichPage
}

