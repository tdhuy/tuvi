const data = require('../service/fetch-data');
const pg = require('pg');
const config = require('config');
const postgresConf = config.get('postgres');
const pool = new pg.Pool(postgresConf);
const myFunc = require('../public/libs/process');
const pay = require('../contant/pay.contant');
const maximumSaved = require('../contant/maximumSaved.contant');
const role = require('../contant/role.contant');
const bcrypt = require('bcrypt');

const getIndexPage = (req, res, next) => {
  	res.send('respond with a resource');
};

const getDataUsers=(req, res, next ) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	if(sessionPhoneNumber==null)
	{
		res.redirect("/");
	}
	else
	{
		data('SELECT * FROM khachhang WHERE userid=$1', [sessionPhoneNumber], (err, result1) =>{
			if(err)
			{
				res.end();
				return console.error("error running query", err);
			}
			res.send(result1.rows);
		});
	}
};

const getDataUser=(req, res, next ) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	if(sessionPhoneNumber==null)
	{
		res.redirect("/");
	}
	else
	{
		let {id}=req.body;
		data('SELECT * FROM khachhang WHERE id=$1', [id], (err, result1) =>{
			if(err)
			{
				res.end();
				return console.error("error running query", err);
			}
			res.send(result1.rows[0]);
		});
	}
};

const getName = (req, res, next ) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	let {ten}= req.body;

	if(sessionPhoneNumber==null)
	{
		res.redirect("/");
	}
	else
	{
		data('UPDATE nguoidung set ten=$1 where sdt = $2', [ten, sessionPhoneNumber], (err, result1) =>{
			if(err)
			{
				res.send("Sửa tên thất bại")
				res.end();
				return console.error("error running query", err);
			}
			res.send("Sửa thành công");
		});
	}
};

const deleteUser = (req, res, next ) => {
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	let { id }= req.body;

	if(sessionPhoneNumber==null)
	{
		res.redirect("/");
	}
	else
	{
		data('DELETE FROM khachhang where id = $1', [id], (err, result1) =>{
			if(err)
			{
				res.send("Xóa thất bại")
				res.end();
				return console.error("error running query", err);
			}
			//console.log(result1.rows);
			res.send("Xóa thành công");
		});
	}
};

const getSuaThongTinPage = (req, res, next) => 
{
	const sessionPhoneNumber=req.session.sdt;

	if(sessionPhoneNumber==null)
	{
		res.redirect("/");
	}
	else
	{
		res.render("sua-thong-tin");
	}

}

const postSuaThongTinPage = (req, res, next) => 
{
	const sessionRole = req.session.role;
	const sessionPay = req.session.pay;
	const sessionPhoneNumber=req.session.sdt;

	if(sessionPhoneNumber==null)
	{
		res.redirect("/");
	}
	else
	{
		let { KH } = req.body;
		
		req.session.chonKH = Number(KH);

		res.render("sua-thong-tin");
	}
}

const getUser=(req, res, next ) => {
	data('SELECT * FROM khachhang WHERE id=$1', [req.session.chonKH], (err, result1) =>{
		if(err)
		{
			res.end();
			return console.error("error running query", err);
		}
		res.send(result1.rows[0]);
	});
};

const getSaveInfoPage = (req, res, next)=>{
	const sessionPhoneNumber=req.session.sdt;

	if(sessionPhoneNumber==null)
	{
		res.redirect("/");
	}
	else
	{	
		res.render("luu-thong-tin");
	}
}

const saveUser= (req, res, next) =>{
	const sessionPhoneNumber=req.session.sdt;

	if(sessionPhoneNumber==null)
	{
		res.redirect("/");
	}
	else
	{
		let ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong, ngayAm, thangAm, namAm, canAm, chiAm, conThu, 
		namSinhCha, namSinhMe, namSinhDoiPhuong, namSinhMeDoiPhuong, namSinhChaDoiPhuong, soCon, namSinhCacCon, namKetHon, conThuDP, gioiTinhCacCon;

		ten=req.body.ten;
		gioiTinh=req.body.gioiTinh;
		gio=req.body.gio;
		ngayDuong=Number(req.body.ngayDuong);
		thangDuong=Number(req.body.thangDuong);
		namDuong=Number(req.body.namDuong);
		ngayAm=Number(req.body.ngayAm);
		thangAm=Number(req.body.thangAm);
		namAm=Number(req.body.namAm);
		canAm=req.body.canAm;
		chiAm=req.body.chiAm;
		conThu=Number(req.body.conThu);
		namSinhCha=Number(req.body.namSinhCha);
		namSinhMe=Number(req.body.namSinhMe);
		namSinhDoiPhuong=Number(req.body.namSinhDoiPhuong);
		namSinhChaDoiPhuong=Number(req.body.namSinhChaDoiPhuong);
		namSinhMeDoiPhuong=Number(req.body.namSinhMeDoiPhuong);
		soCon=Number(req.body.soCon);
		namSinhCacCon=req.body.namSinhCacCon;
		namKetHon=Number(req.body.namKetHon);
		conThuDP=Number(req.body.conThuDP);
		gioiTinhCacCon=req.body.gioiTinhCacCon;
		let date = new Date();	

		console.log(req.session.chonKH);

		data('UPDATE khachhang SET ten=$1, gioitinh = $2, giohoangdao = $3, ngayduong = $4, thangduong = $5, namduong = $6, ngayam = $7, thangam = $8, namam = $9, canam = $10, chiam = $11, conthu = $12, namsinhcha = $13, namsinhme = $14, namsinhdoiphuong = $15, namsinhchadoiphuong = $16, namsinhmedoiphuong = $17, socon = $18, namsinhcaccon = $19, namkethon = $20, conthudp = $21, gioitinhcaccon = $22, userid = $23, updated_at = $24 WHERE id = $25',
		 [ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong, ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe, namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon, namKetHon, conThuDP, gioiTinhCacCon, sessionPhoneNumber, date, req.session.chonKH], (err, result) =>{
			if(err){
				res.send("Sửa thất bại")
				res.end();
				return console.error("error running query", err);
			}
			res.send("Sửa thành công");
		}); 
	}
}

const addContent = (req, res, next) =>{
	let {sdt, content} = req.body;
	let date = new Date();

	if(sdt=="")
	{
		res.send("Hãy nhập số điện thoại!");
	}
	else if(sdt.length != 10)
	{
		res.send("Số điện thoại phải là 10 số! Vui lòng kiểm tra lại.");
	}
	else
	{
		let hour = date.getHours(), minute = date.getMinutes(), day = date.getDate(), month = (date.getMonth() + 1);

		if(hour < 10)
		{
			hour = "0" + hour;
		}

		if(minute < 10)
		{
			minute = "0" + minute;
		}

		if(day < 10)
		{
			day = "0" + day;
		}

		if(month < 10)
		{
			month = "0" + month;  
		}

		let time= hour + ":" + minute + " " + day + "/" + month + "/" + date.getFullYear();
		data('INSERT INTO lienhe(sdt, noidung, ngaygui, dalienhe) VALUES($1,$2,$3,$4)',[sdt, content, time, 1], (err, result) =>{
			if(err){
				res.send("Gửi thất bại");
				res.end();
				return console.error("error running query", err);
			}
			res.send("Gửi thành công");
		}); 
	}
}

module.exports = {
  getIndexPage,
  getDataUsers,
  deleteUser,
  getSuaThongTinPage,
  getSaveInfoPage,
  getDataUser,
  getUser,
  postSuaThongTinPage,
  saveUser,
  addContent
};