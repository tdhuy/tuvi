const pg = require('pg');
const config = require('config');
const postgresConf = config.get('postgres');
const pool = new pg.Pool(postgresConf);

/**
 * @param {string} queryStr
 * @param {string[]}params
 * @param {Function} cb
 */
module.exports = (queryStr, params, cb) => {
  pool.connect(function (err, client, done) {
    if (err) {
      return console.error("error fetching client from pool", err);
    }

    client.query(queryStr, params, (err, result) => {
      done();
      return cb(err, result);
    });
  });
};