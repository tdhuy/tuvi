const { Pool } = require('pg');
const config = require('config');
const pool = new Pool(config.get('postgres'));

module.exports = (cb) => {
  pool.connect(function (err, client, done) {
    if (err) {
      return cb(false);
    }

    done();
    return cb(true);
  });
};