$(document).ready(function(){
	var dataTime;
	var i=0;

	function canChiGio(chiGio, canNgay) {
	var dsCan = ["Giáp","Ất","Bính","Đinh","Mậu","Kỷ","Canh","Tân","Nhâm","Quý"];
	var dsChi = ["Tý","Sửu","Dần","Mẹo","Thìn","Tỵ","Ngọ","Mùi","Thân","Dậu","Tuất","Hợi"];
	var dsCanGioDau = {
		"Giáp": 0,
		"Kỷ": 0,
		"Ất": 2,
		"Canh": 2,
		"Bính": 4,
		"Tân": 4,
		"Đinh": 6,
		"Nhâm": 6,
		"Mậu": 8,
		"Quý": 8
	};
	var chiSoCanGioDau = dsCanGioDau[canNgay];
	var count = 0;

	for(var chi of dsChi)
		if(chi !== chiGio)
			count++;
		else break;

	var chiSoCanGio = (chiSoCanGioDau + count) % 10;

	return dsCan[chiSoCanGio] + " " + chiGio;
}
	
	function date_time() 
	{
	    date = new Date;
	    year = date.getFullYear();
	    month = date.getMonth();
	    d = date.getDate();
	    months = new Array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
	    day = date.getDay();
	    days = new Array('Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy');
	    h = date.getHours();
	    if (h < 10) {
	        h = "0" + h;
	    }
	    m = date.getMinutes();
	    if (m < 10) {;

	        m = "0" + m    }
	    s = date.getSeconds();
	    if (s < 10) {
	        s = "0" + s;
	    }

	    var gio="";
	    $.post("/ban-than/gettime",{
		month: month+1
		},function (data)
		{
			data.forEach(function(val){
				if(val.gioketthuc==h && val.phutketthuc >= m)
				{
					gio= val.khunggio;
					return gio;
				}
				else if(val.giobatdau==h && val.phutbatdau <= m)
				{
					gio= val.khunggio;
					return gio;
				}
				else if(val.gioketthuc==0)
				{
					if(val.giobatdau < h)
					{
						gio= val.khunggio;
						return gio;
					}
				}
				else if(val.giobatdau < h &&  h < val.gioketthuc)
				{
					gio= val.khunggio;
					return gio;
				}
			});

			var amLich=convertSolar2Lunar(d, month+1, year, 7)
		    var jd=jdn(d, month+1, year);
		    var canChiNgayThangNam = getCanChi(amLich[0], amLich[1], amLich[2], amLich[3], jd);

		    var chi = gio.split(" ");
		    var canNgay = canChiNgayThangNam[0].split(" ");

		    var gioCanChi = canChiGio(chi[0], canNgay[0]); 

		    result ='Bây giờ là ' + h + ':' + m + ', ngày ' + d + '/' + months[month] + '/' + year + ' (G.'+ gioCanChi + ", Ng." + canChiNgayThangNam[0] + ', T.' + canChiNgayThangNam[1] + ', N.' + canChiNgayThangNam[2] + ')';
		    var ele  = document.getElementById("date_time");
		    ele.innerHTML = result;

		    setTimeout(date_time, 1000 * 60);
		});
	};

	date_time();
});	