var CAN = new Array("Gi\341p", "\u1EA4t", "B\355nh", "\u0110inh", "M\u1EADu", "K\u1EF7", "Canh", "T\342n", "Nh\342m", "Qu\375");
var CHI = new Array("T\375", "S\u1EEDu", "D\u1EA7n", "M\343o", "Th\354n", "T\u1EF5", "Ng\u1ECD", "M\371i", "Th\342n", "D\u1EADu", "Tu\u1EA5t", "H\u1EE3i");
var TIETKHI = new Array("Xu\u00E2n ph\u00E2n", "Thanh minh", "C\u1ED1c v\u0169", "L\u1EADp h\u1EA1", "Ti\u1EC3u m\u00E3n", "Mang ch\u1EE7ng",
		"H\u1EA1 ch\u00ED", "Ti\u1EC3u th\u1EED", "\u0110\u1EA1i th\u1EED", "L\u1EADp thu", "X\u1EED th\u1EED", "B\u1EA1ch l\u1ED9",
		"Thu ph\u00E2n", "H\u00E0n l\u1ED9", "S\u01B0\u01A1ng gi\u00E1ng", "L\u1EADp \u0111\u00F4ng", "Ti\u1EC3u tuy\u1EBFt", "\u0110\u1EA1i tuy\u1EBFt",
		"\u0110\u00F4ng ch\u00ED", "Ti\u1EC3u h\u00E0n", "\u0110\u1EA1i h\u00E0n", "L\u1EADp xu\u00E2n", "V\u0169 Th\u1EE7y", "Kinh tr\u1EADp");
var CUNGPHINAM=new Array("Khôn", "Khảm", "Ly", "Cấn", "Đoài", "Càn", "Khôn", "Tốn", "Chấn");
var CUNGPHINU= new Array("Tốn", "Cấn", "Càn", "Đoài", "Cấn", "Ly", "Khảm", "Khôn", "Chấn");
var MENHPHONGTHUYNAM=new Array("Tây mệnh", "Đông mệnh", "Đông mệnh", "Tây mệnh", "Tây mệnh", "Tây mệnh", "Tây mệnh", "Đông mệnh", "Đông mệnh");
var MENHPHONGTHUYNU= new Array("Đông mệnh", "Tây mệnh", "Tây mệnh", "Tây mệnh", "Tây mệnh", "Đông mệnh", "Đông mệnh", "Tây mệnh", "Đông mệnh");
var NGUHANH = new Array("Kim", "Thủy", "Hỏa", "Thổ", "Mộc");


function timChiSoChi(chi)
{
	if(chi == "Tý" || chi == "Sửu" || chi == "Ngọ" || chi == "Mùi")
	{
		return 1;
	}
	else if(chi =="Dần" || chi == "Mão" || chi == "Thân" || chi == "Dậu")
	{
		return 2;
	}
	else if (chi == "Thìn" || chi == "Tỵ" || chi == "Tuất" || chi == "Hợi")
	{
		return 3;
	}

}

function timChiSoCan(can)
{
	if(can == "Giáp" || can == "Ất")
	{
		return 0;
	}
	else if(can == "Bính" || can == "Đinh")
	{
		return 1;
	}
	else if(can == "Mậu" || can == "Kỷ")
	{
		return 2;
	}
	else if(can == "Canh" || can == "Tân")
	{
		return 3;
	}
	else if(can == "Nhâm" || can == "Quý")
	{
		return 4;
	}
}

function timNguHanh(can, chi)
{
	var chiSoChi = timChiSoChi(chi);
	var chiSoCan = timChiSoCan(can);
	var nguHanh = chiSoChi + chiSoCan;

	if(nguHanh>5)
	{
		nguHanh%=5;
		return NGUHANH[nguHanh-1];
	}
	else
	{
		return NGUHANH[nguHanh-1];
	}
}

function timCanGio(canNgay)
{
	if(canNgay=="Giáp" || canNgay=="Kỷ")
	{
		return new Array("Giáp", "Ất", "Bính", "Đinh", "Mậu", "Kỷ", "Canh", "Tân", "Nhâm", "Quý", "Giáp", "Ất"); 
	}
	else if(canNgay=="Ất"||canNgay=="Canh")
	{
		return new Array("Bính", "Đinh", "Mậu", "Kỷ", "Canh", "Tân", "Nhâm", "Quý", "Giáp", "Ất", "Bính", "Đinh"); 
	}
	else if(canNgay=="Bính"||canNgay=="Tân")
	{
		return new Array("Mậu", "Kỷ", "Canh", "Tân", "Nhâm", "Quý", "Giáp", "Ất", "Bính", "Đinh", "Mậu", "Kỷ"); 
	}
	else if(canNgay=="Đinh" || canNgay=="Nhâm")
	{
		return new Array("Canh", "Tân", "Nhâm", "Quý", "Giáp", "Ất", "Bính", "Đinh", "Mậu", "Kỷ", "Canh", "Tân");
	}
	else
	{
		return new Array("Nhâm", "Quý", "Giáp", "Ất", "Bính", "Đinh", "Mậu", "Kỷ", "Canh", "Tân", "Nhâm", "Quý");
	}
}

function timngay(_month,_year){
	var _day;

    if (_month == 1 || _month == 3 || _month == 5 || _month == 7 || _month == 8 || _month == 10 || _month == 12)
    {
    	_day = 31;
    }
    else if(_month == 4 || _month == 6 || _month == 9 || _month == 11)
    {
    	_day = 30;
    }
    else
    {
    	if((_year % 4 == 0 &&_year % 100 != 0) || _year % 400 == 0)
    	{
            _day = 29;
    	}
        else
        {
        	_day = 28;
        }
              
    }     
    return _day;
}

function ngaySau(_day,_month,_year)
{
    _day = _day + 1;

   	if (_day >timngay(_month,_year))
   	{
   		_day = 1;
       	_month = _month +1;

        if(_month >12)
        {
        	_day = 1;
            _month = 1;
            _year = _year + 1;
        }         
   	}
   	return new Array(_day, _month, _year);       
}

function tinhNhieuNgay(_day,_month,_year, soNgay)
{
	var a=_day;
	var b=_month;
	var c=_year;
	var d;

	for (var i =0; i< soNgay; i++) {	
		d=ngaySau(a,b,c);
		a=d[0];
		b=d[1];
		c=d[2];
	}
	return d; 
}

function timThangDuThangThieu(_day,_month,_year)
{
	var amLich=convertSolar2Lunar(_day, _month, _year, 7);

	if(amLich[0]==29)
	{
		var ngaysau= ngaySau(_day, _month, _year);
		var amLich2= convertSolar2Lunar(ngaysau[0], ngaysau[1], ngaysau[2], 7);
		if(amLich2[0]==1)
		{
			alert("có 29 ngày");
		}
		else
		{
			alert("có 30 ngày");
		}
	}
	else if(amLich[0]==30)
	{
		alert("có 30 ngày");
	}
	else
	{
		//alert(amLich);
		var soNgay=29-amLich[0];
		//alert(soNgay);
		var ngay = tinhNhieuNgay(_day, _month, _year, soNgay);
		//alert(ngay);
		var ngaysau= ngaySau(ngay[0], ngay[1], ngay[2]);
		var amLich2= convertSolar2Lunar(ngaysau[0], ngaysau[1], ngaysau[2], 7);
		//alert(amLich2);

		if(amLich2[0]==1)
		{
			alert("có 29 ngày");
		}
		else
		{
			alert("có 30 ngày");
		}
	}
}   

var PI = Math.PI;

	function INT(d) {
		return Math.floor(d);
	}

	function jdFromDate(dd, mm, yy) {
		var a, y, m, jd;
		a = INT((14 - mm) / 12);
		y = yy+4800-a;
		m = mm+12*a-3;
		jd = dd + INT((153*m+2)/5) + 365*y + INT(y/4) - INT(y/100) + INT(y/400) - 32045;
		if (jd < 2299161) {
			jd = dd + INT((153*m+2)/5) + 365*y + INT(y/4) - 32083;
		}
		return jd;
	}

	function jdToDate(jd)
	{
		var a, b, c, d, e, m, day, month, year;
		if (jd > 2299160) { // After 5/10/1582, Gregorian calendar
			a = jd + 32044;
			b = INT((4*a+3)/146097);
			c = a - INT((b*146097)/4);
		} else {
			b = 0;
			c = jd + 32082;
		}
		d = INT((4*c+3)/1461);
		e = c - INT((1461*d)/4);
		m = INT((5*e+2)/153);
		day = e - INT((153*m+2)/5) + 1;
		month = m + 3 - 12*INT(m/10);
		year = b*100 + d - 4800 + INT(m/10);
		return new Array(day, month, year);
	}

	function NewMoon(k) {
		var T, T2, T3, dr, Jd1, M, Mpr, F, C1, deltat, JdNew;
		T = k/1236.85; 
		T2 = T * T;
		T3 = T2 * T;
		dr = PI/180;
		Jd1 = 2415020.75933 + 29.53058868*k + 0.0001178*T2 - 0.000000155*T3;
		Jd1 = Jd1 + 0.00033*Math.sin((166.56 + 132.87*T - 0.009173*T2)*dr); 
		M = 359.2242 + 29.10535608*k - 0.0000333*T2 - 0.00000347*T3; 
		Mpr = 306.0253 + 385.81691806*k + 0.0107306*T2 + 0.00001236*T3; 
		F = 21.2964 + 390.67050646*k - 0.0016528*T2 - 0.00000239*T3; 
		C1=(0.1734 - 0.000393*T)*Math.sin(M*dr) + 0.0021*Math.sin(2*dr*M);
		C1 = C1 - 0.4068*Math.sin(Mpr*dr) + 0.0161*Math.sin(dr*2*Mpr);
		C1 = C1 - 0.0004*Math.sin(dr*3*Mpr);
		C1 = C1 + 0.0104*Math.sin(dr*2*F) - 0.0051*Math.sin(dr*(M+Mpr));
		C1 = C1 - 0.0074*Math.sin(dr*(M-Mpr)) + 0.0004*Math.sin(dr*(2*F+M));
		C1 = C1 - 0.0004*Math.sin(dr*(2*F-M)) - 0.0006*Math.sin(dr*(2*F+Mpr));
		C1 = C1 + 0.0010*Math.sin(dr*(2*F-Mpr)) + 0.0005*Math.sin(dr*(2*Mpr+M));
		if (T < -11) {
			deltat= 0.001 + 0.000839*T + 0.0002261*T2 - 0.00000845*T3 - 0.000000081*T*T3;
		} else {
			deltat= -0.000278 + 0.000265*T + 0.000262*T2;
		};
		JdNew = Jd1 + C1 - deltat;	
		return JdNew;
	}

	function SunLongitude(jdn) {
		var T, T2, dr, M, L0, DL, L;
		T = (jdn - 2451545.0 ) / 36525; 
		T2 = T*T;
		dr = PI/180; 
		M = 357.52910 + 35999.05030*T - 0.0001559*T2 - 0.00000048*T*T2; 
		L0 = 280.46645 + 36000.76983*T + 0.0003032*T2; 
		DL = (1.914600 - 0.004817*T - 0.000014*T2)*Math.sin(dr*M);
		DL = DL + (0.019993 - 0.000101*T)*Math.sin(dr*2*M) + 0.000290*Math.sin(dr*3*M);
		L = L0 + DL; 
		L = L*dr;
		L = L - PI*2*(INT(L/(PI*2))); 
		return L;
	}


	function getSunLongitude(dayNumber, timeZone) {
		return INT(SunLongitude(dayNumber - 0.5 - timeZone/24)/PI*6);
	}

	function getNewMoonDay(k, timeZone) {
		return INT(NewMoon(k) + 0.5 + timeZone/24);
	}


	function getLunarMonth11(yy, timeZone) {
		var k, off, nm, sunLong;

		off = jdFromDate(31, 12, yy) - 2415021;
		k = INT(off / 29.530588853);
		nm = getNewMoonDay(k, timeZone);
		sunLong = getSunLongitude(nm, timeZone); 
		if (sunLong >= 9) {
			nm = getNewMoonDay(k-1, timeZone);
		}
		return nm;
	}

	function getLeapMonthOffset(a11, timeZone) {
		var k, last, arc, i;
		k = INT((a11 - 2415021.076998695) / 29.530588853 + 0.5);
		last = 0;
		i = 1; 
		arc = getSunLongitude(getNewMoonDay(k+i, timeZone), timeZone);
		do {
			last = arc;
			i++;
			arc = getSunLongitude(getNewMoonDay(k+i, timeZone), timeZone);
		} while (arc != last && i < 14);
		return i-1;
	}

	function convertSolar2Lunar(dd, mm, yy, timeZone) {
		var k, dayNumber, monthStart, a11, b11, lunarDay, lunarMonth, lunarYear, lunarLeap;
		dayNumber = jdFromDate(dd, mm, yy);
		k = INT((dayNumber - 2415021.076998695) / 29.530588853);
		monthStart = getNewMoonDay(k+1, timeZone);
		if (monthStart > dayNumber) {
			monthStart = getNewMoonDay(k, timeZone);
		}
		a11 = getLunarMonth11(yy, timeZone);
		b11 = a11;
		if (a11 >= monthStart) {
			lunarYear = yy;
			a11 = getLunarMonth11(yy-1, timeZone);
		} else {
			lunarYear = yy+1;
			b11 = getLunarMonth11(yy+1, timeZone);
		}
		lunarDay = dayNumber-monthStart+1;
		diff = INT((monthStart - a11)/29);
		lunarLeap = 0;
		lunarMonth = diff+11;
		if (b11 - a11 > 365) {
			leapMonthDiff = getLeapMonthOffset(a11, timeZone);
			if (diff >= leapMonthDiff) {
				lunarMonth = diff + 10;
				if (diff == leapMonthDiff) {
					lunarLeap = 1;
				}
			}
		}
		if (lunarMonth > 12) {
			lunarMonth = lunarMonth - 12;
		}
		if (lunarMonth >= 11 && diff < 4) {
			lunarYear -= 1;
		}
		return new Array(lunarDay, lunarMonth, lunarYear, lunarLeap);
	}

	function convertLunar2Solar(lunarDay, lunarMonth, lunarYear, lunarLeap, timeZone)
	{
		var k, a11, b11, off, leapOff, leapMonth, monthStart;
		if (lunarMonth < 11) {
			a11 = getLunarMonth11(lunarYear-1, timeZone);
			b11 = getLunarMonth11(lunarYear, timeZone);
		} else {
			a11 = getLunarMonth11(lunarYear, timeZone);
			b11 = getLunarMonth11(lunarYear+1, timeZone);
		}
		off = lunarMonth - 11;
		if (off < 0) {
			off += 12;
		}
		if (b11 - a11 > 365) {
			leapOff = getLeapMonthOffset(a11, timeZone);
			leapMonth = leapOff - 2;
			if (leapMonth < 0) {
				leapMonth += 12;
			}
			if (lunarLeap != 0 && lunarMonth != leapMonth) {
				return new Array(0, 0, 0);
			} else if (lunarLeap != 0 || off >= leapOff) {
				off += 1;
			}
		}
		k = INT(0.5 + (a11 - 2415021.076998695) / 29.530588853);
		monthStart = getNewMoonDay(k+off, timeZone);
		return jdToDate(monthStart+lunarDay-1);
	}

	function getYearCanChi(year) {
		return CAN[(year+6) % 10] + " " + CHI[(year+8) % 12];
	}

	function getYearCanChi1(year) {
		return new Array(CAN[(year+6) % 10], CHI[(year+8) % 12]);
	}

	function jdn(dd, mm, yy) {
		var a = INT((14 - mm) / 12);
		var y = yy+4800-a;
		var m = mm+12*a-3;
		var jd = dd + INT((153*m+2)/5) + 365*y + INT(y/4) - INT(y/100) + INT(y/400) - 32045;
		return jd;
		//return 367*yy - INT(7*(yy+INT((mm+9)/12))/4) - INT(3*(INT((yy+(mm-9)/7)/100)+1)/4) + INT(275*mm/9)+dd+1721029;
	}

	function getCanChi(day, month, year, leap, jd) {
		var dayName, monthName, yearName;

		dayName = CAN[(jd + 9) % 10] + " " + CHI[(jd+1)%12];
		monthName = CAN[(year*12+month+3) % 10] + " " + CHI[(month+1)%12];
		if (leap == 1) {
			monthName += " (nhu\u1EADn)";
		}
		yearName = getYearCanChi(year);
		return new Array(dayName, monthName, yearName);
	}

	function SunLongitude1(jdn) {
		var T, T2, dr, M, L0, DL, lambda, theta, omega;
		T = (jdn - 2451545.0 ) / 36525; // Time in Julian centuries from 2000-01-01 12:00:00 GMT
		T2 = T*T;
		dr = PI/180; // degree to radian
		M = 357.52910 + 35999.05030*T - 0.0001559*T2 - 0.00000048*T*T2; // mean anomaly, degree
		L0 = 280.46645 + 36000.76983*T + 0.0003032*T2; // mean longitude, degree
		DL = (1.914600 - 0.004817*T - 0.000014*T2)*Math.sin(dr*M);
		DL = DL + (0.019993 - 0.000101*T)*Math.sin(dr*2*M) + 0.000290*Math.sin(dr*3*M);
	    theta = L0 + DL; // true longitude, degree
	    // obtain apparent longitude by correcting for nutation and aberration
	    omega = 125.04 - 1934.136 * T;
	    lambda = theta - 0.00569 - 0.00478 * Math.sin(omega * dr);
	    // Convert to radians
	    lambda = lambda*dr;
		lambda = lambda - PI*2*(INT(lambda/(PI*2))); // Normalize to (0, 2*PI)
	    return lambda;
	}

	function getSunLongitude1(dayNumber, timeZone) {
		return INT(SunLongitude1(dayNumber - 0.5 - timeZone/24.0) / PI * 12);
	}
