$(document).ready(function() {
	var basicData=[];

	layQue();

	$.post('/kinh-dich/layDuLieu', function(data){

		var thanChu = $("#thanchu");

		var createElement=jQuery('<option>'+"- Chọn người xem -" +'</option>');
		createElement.attr("value", "-1");
		thanChu.append(createElement);

		if(data != null)
		{
			data.forEach(function(val, item){
				var createElement=jQuery('<option>'+ val.ten + " " + val.namam +'</option>');
				createElement.attr("value", item);
				basicData.push(val);
				thanChu.append(createElement);
			});
		}
	});

	function chonTenQue(que, ele)
	{
		if(que=="+++")
		{
			ele.text("Lão Dương");
		}
		else if(que=="---")
		{
			ele.text("Lão Âm");
		}
		else if(que=="++-")
		{
			ele.text("Thiếu Dương");
		}
		else
		{
			ele.text("Thiếu Âm");
		}
	}

	$("#chon-hao1").change(function(){
		chonTenQue($("#chon-hao1 option:selected").text(), $("#ten-hao1"));
		hienGiaoDienQue($("#chon-hao1"), $("#hao-1-1"), $('#hao-1-2'));
		layQue();
	});

	$("#chon-hao2").change(function(){
		chonTenQue($("#chon-hao2 option:selected").text(), $("#ten-hao2"));
		hienGiaoDienQue($("#chon-hao2"), $("#hao-2-1"), $('#hao-2-2'));
		layQue();
	});

	$("#chon-hao3").change(function(){
		chonTenQue($("#chon-hao3 option:selected").text(), $("#ten-hao3"));
		hienGiaoDienQue($("#chon-hao3"), $("#hao-3-1"), $('#hao-3-2'));
		layQue();
	});

	$("#chon-hao4").change(function(){
		chonTenQue($("#chon-hao4 option:selected").text(), $("#ten-hao4"));
		hienGiaoDienQue($("#chon-hao4"), $("#hao-4-1"), $('#hao-4-2'));
		layQue();
	});

	$("#chon-hao5").change(function(){
		chonTenQue($("#chon-hao5 option:selected").text(), $("#ten-hao5"));
		hienGiaoDienQue($("#chon-hao5"), $("#hao-5-1"), $('#hao-5-2'));
		layQue();
	});

	$("#chon-hao6").change(function(){
		chonTenQue($("#chon-hao6 option:selected").text(), $("#ten-hao6"));
		hienGiaoDienQue($("#chon-hao6"), $("#hao-6-1"), $('#hao-6-2'));
		layQue();
	});

	function hienGiaoDienQue(the, doHoa, doHoa1)
	{
		if(the.val()=="1")
		{
			doHoa.show();
			doHoa1.hide();
		}
		else
		{
			doHoa1.show();
			doHoa.hide();
		}
	}

	chonTenQue($("#chon-hao1 option:selected").text(), $("#ten-hao1"));
	hienGiaoDienQue($("#chon-hao1"), $("#hao-1-1"), $('#hao-1-2'));
	chonTenQue($("#chon-hao2 option:selected").text(), $("#ten-hao2"));
	hienGiaoDienQue($("#chon-hao2"), $("#hao-2-1"), $('#hao-2-2'));
	chonTenQue($("#chon-hao3 option:selected").text(), $("#ten-hao3"));
	hienGiaoDienQue($("#chon-hao3"), $("#hao-3-1"), $('#hao-3-2'));
	chonTenQue($("#chon-hao4 option:selected").text(), $("#ten-hao4"));
	hienGiaoDienQue($("#chon-hao4"), $("#hao-4-1"), $('#hao-4-2'));
	chonTenQue($("#chon-hao5 option:selected").text(), $("#ten-hao5"));
	hienGiaoDienQue($("#chon-hao5"), $("#hao-5-1"), $('#hao-5-2'));
	chonTenQue($("#chon-hao6 option:selected").text(), $("#ten-hao6"));
	hienGiaoDienQue($("#chon-hao6"), $("#hao-6-1"), $('#hao-6-2'));

	function layQue()
	{
		var value=$("#thanchu").val();


		var que = $("#chon-hao4").val() + "" + $("#chon-hao5").val() + "" + $("#chon-hao6").val() + "" + $("#chon-hao1").val() + "" + $("#chon-hao2").val() + "" + $("#chon-hao3").val();

		$.post('/kinh-dich/layKinhDich',
		{
			que:que
		},function(data){
			$("#tenque").html("<p>" + data.ten + "</p>");
			$("#ynghiaque").text(data.ynghia);
			//alert(data.ten + " " + data.ynghia);	
		});	
		// if(value !="-1")
		// {
		// 	// if(basicData[value].gioitinh=="Nam")
		// 	// {
		// 	// 	$("#gioiTinh").val("Nam");
		// 	// }
		// 	// else
		// 	// {
		// 	// 	$("#gioiTinh").val("Nữ");
		// 	// }

		
 	// 	}
 	// 	else
 	// 	{
 	// 		$("#tenque").html("<p></p>");
		// 	$("#ynghiaque").text("");
 	// 	}		
	}

	// $("#thanchu").change(function(){
	// 	layQue();
	// });
});