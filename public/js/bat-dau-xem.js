$(document).ready(function(){
	var nam, nu, danhSachNam, danhSachNu, namHienTai, canChi;

	namHienTai=new Date();
	danhSachNam=$("#hoten-namsinh-nam");
	danhSachNu=$("#hoten-namsinh-nu");
	canChi=getYearCanChi1(namHienTai.getFullYear());


	// var namxem=$('#namxem');
	// for(var i =1900 ; i<2080;i++)
	// {
	// 	var createElement=jQuery('<option>'+ i +'</option>');
	// 		createElement.attr("value", i);
	// 		namxem.append(createElement);
	// }
	if($("#alert").text() != "")
	{
		alert($("#alert").text());
	}

	$('#namxem').val(namHienTai.getFullYear());

	$.post("/batdauxem/nam",
	function(data){
		$.post("/batdauxem/layDuLieu",function(data1){
			nam=[];
			var i=0;
			danhSachNam.empty();

			var createElement=jQuery('<option>'+"- Chọn người xem -" +'</option>');
			createElement.attr("value", "-1");
			danhSachNam.append(createElement);
			data.forEach(function(val, item)
			{
				createElement=jQuery('<option>'+ data[item].ten +"  "+data[item].namduong +'</option>');
				createElement.attr("value", item);
				danhSachNam.append(createElement);
				nam.push(data[item]);
				i=item+1;
			});

			if(data1!="0")
			{
				printBasicInfo(data1, i, danhSachNam, nam, "Nam", namHienTai, getMaleInfo);
				 $("#xemve").val("1")
			}
		});
	});

	$.post("/batdauxem/nu",
	function(data1){
		$.post("/batdauxem/layDuLieu",function(data){
			nu=[];
			var i=0;
			danhSachNu.empty();

			var createElement=jQuery('<option>'+"- Chọn người xem -" +'</option>');
			createElement.attr("value", "-1");
			danhSachNu.append(createElement);
			data1.forEach(function(val, item)
			{
				createElement=jQuery('<option>'+ data1[item].ten +"  "+data1[item].namduong +'</option>');
				createElement.attr("value", item);
				danhSachNu.append(createElement);
				nu.push(data1[item]);
				i=item+1;
			});

			if(data!="0")
			{
				printBasicInfo(data, i, danhSachNu, nu, "Nữ", namHienTai, getFeMaleInfo);
				$("#xemve").val("1")	
			}
		});
	});

	//hàm lấy thông tin cơ bản
	function printBasicInfo(data, i, list , sexInfo, sex, year, func)
	{
		data.forEach(function(val, item){
			if(val.gioitinh == sex)
			{
				if(item < 2)
				{
					createElement=jQuery('<option>'+ val.ten +"  "+ val.namduong +'</option>');
					createElement.attr("value", i);
					list.append(createElement);

					sexInfo.push(val);
				}
			}
		});
	}

	//khi lưa chọn bên nam thay dổi;
	$("#hoten-namsinh-nam").change(function(){
		var luachon= $("#xemve").val();
		showAllInfo(nam, nu, namxem, luachon);
	});

	//khi lựa chọn bên nữ thay đổi
	$("#hoten-namsinh-nu").change(function(){
		var luachon= $("#xemve").val();
		showAllInfo(nam, nu, namxem, luachon);
	})

	$('#namxem').change(function(){
		var luachon= $("#xemve").val();
		showAllInfo(nam, nu, namxem, luachon);
	});

	//tạo bảng 
	function createTable(title, val1, val2)
	{
		var bang =$('#bangchitiet');

		var Element=jQuery("<tr></tr>");
		var createElement1=jQuery('<td>' + title + "</td>");
		var createElement2=jQuery('<td>' + val1 + "</td>");
		var createElement3=jQuery('<td>' + val2 + "</td>");
		Element.append(createElement1);
		Element.append(createElement2);
		Element.append(createElement3);

		bang.append(Element);
	}


	function createTableRows(rows) {
		rows.forEach(function(row){
			createTable(row[0], row[1], row[2]);
		});
	}

	//lấy thông tin cơ bản bên nữ
	function getFeMaleInfo(nu, ttNu, namxem)
	{
	//khi năm sinh âm lớn hơn năm xem
		if(nu[ttNu].namam > namxem)
		{
			alert("Năm xem đang nhỏ hơn Năm sinh. Vui lòng chọn lại Năm xem!");
			//$("#xemve").val("0");
			//$("#xemve option:selected").text("Cơ bản");
		}
		//khi khoảng cách giữa năm sinh âm và năm xem lớn hơn 120 năm
		else if((namxem - nu[ttNu].namam + 1) >120)
		{
			alert("Khoảng cách giữa Năm xem và Năm sinh lớn hơn 120 năm. Vui lòng chọn lại Năm xem!");
			//$("#xemve").val("0");
			//$("#xemve option:selected").text("Cơ bản");
		}
		else
		{
			$("#tieudeXemVe").text("");
			$("#tieudeNu").text("NỮ");
			$("#tieudeNam").text("");

			$.post("/ban-than/getDetail",{
			year: nu[ttNu].namam
			} 
			,function(data2)
			{
				var rows = [
					["Tên:", "", nu[ttNu].ten],
					["Tuổi:", "", namxem - nu[ttNu].namam + 1],
					["Con thứ:", "", nu[ttNu].conthu],
					["Năm:", "", nu[ttNu].canam+" "+nu[ttNu].chiam],
					["Mạng:", "", data2.mang + " (" + data2.ynm + ")"],
					["Mạng khắc:", "", data2.mangkhac],
					["Cung sanh:", "", data2.cungsanh],
					["Cung phi:", "", CUNGPHINU[(nu[ttNu].namam%9)]],
					["Mệnh phong thủy:", "", MENHPHONGTHUYNU[nu[ttNu].namam%9]],
					["Con nhà:", "", data2.connha + " (" + data2.yncn + ")"],
					["Xương:", "", data2.xuong],
					["Tướng tinh:", "" , data2.tuongtinh],
					["Độ mạng:", "" , data2.domangnu],
				];

				if(nu[ttNu].namkethon!= 0)
				{
					rows.push(["Năm kết hôn: ", "",nu[ttNu].namkethon]);
					if(nu[ttNu].namsinhdoiphuong != 0)
					{
						rows.push(["Năm sinh vợ /chồng: ", "",nu[ttNu].namsinhdoiphuong + " (" + (namxem - Number(nu[ttNu].namsinhdoiphuong) + 1) + "t)"]);
					}
					else
					{
						rows.push(["Năm sinh vợ /chồng: ", "", "----"]);
					}					
				}
				else
				{
					rows.push(["Năm kết hôn: ", "", "----"]);
					rows.push(["Năm sinh vợ /chồng: ", "", "----"]);
				}

				if(nu[ttNu].thangam == 0)
				{
					rows.push(["TS-ĐS: ", "", "Vui lòng nhập đủ ngày tháng sanh!"]);
					createTableRows(rows);
				}
				else
				{
					//alert(timNguHanh(nu[ttNu].canam, nu[ttNu].chiam));

					$.post("/batdauxem/tsds",
					{
						nguHanh: timNguHanh(nu[ttNu].canam, nu[ttNu].chiam),
						thang: nu[ttNu].thangam
					}
					,function(data){
						rows.push(["TS-ĐS: ", "", data.tsds]);
						createTableRows(rows);
					});
				}
			});					
		}
	}

	//lấy thông tin nam
	function getMaleInfo(nam, ttNam, namxem)
	{
		//khi năm sinh âm lớn hơn năm xem
		if(nam[ttNam].namam > namxem )
		{
			alert("Năm xem đang nhỏ hơn Năm sinh. Vui lòng chọn lại Năm xem!");
			//$("#xemve").val("0");
			//$("#xemve option:selected").text("Cơ bản");
		}
		//khi khoảng cách giữa năm sinh âm và năm xem lớn hơn 120 năm
		else if((namxem-nam[ttNam].namam +1) >120)
		{
			alert("Khoảng cách giữa Năm xem và Năm sinh lớn hơn 120 năm. Vui lòng chọn lại Năm xem!");
			//$("#xemve").val("0");
			//$("#xemve option:selected").text("Cơ bản");
		}
		else
		{
			$("#tieudeXemVe").text("");
			$("#tieudeNu").text("");
			$("#tieudeNam").text("NAM");
			
			$.post("/ban-than/getDetail",{
			year: nam[ttNam].namam
			} 
			,function(data2)
			{
				var rows = [
					["Tên:", nam[ttNam].ten, ""],
					["Tuổi:",namxem - nam[ttNam].namam +1, ""],
					["Con thứ:", nam[ttNam].conthu, ""],
					["Năm:",nam[ttNam].canam+" "+nam[ttNam].chiam, ""],
					["Mạng:", data2.mang + " (" + data2.ynm + ")", ""],
					["Mạng khắc:", data2.mangkhac, ""],
					["Cung sanh:", data2.cungsanh, ""],
					["Cung phi:", CUNGPHINAM[(nam[ttNam].namam%9)], ""],
					["Mệnh phong thủy:", MENHPHONGTHUYNAM[nam[ttNam].namam % 9], ""],
					["Con nhà:", data2.connha + " (" + data2.yncn + ")", ""],
					["Xương:", data2.xuong , ""],
					["Tướng tinh:", data2.tuongtinh , ""],
					["Độ mạng:", data2.domangnam , ""]
				];

				if(nam[ttNam].namkethon!=0)
				{
					rows.push(["Năm kết hôn: ", nam[ttNam].namkethon, ""]);

					if(nam[ttNam].namsinhdoiphuong !=0)
					{
						rows.push(["Năm sinh vợ /chồng: ", nam[ttNam].namsinhdoiphuong+ " (" + (namxem - Number(nam[ttNam].namsinhdoiphuong) + 1) + "t)", ""]);
					}
					else
					{
						rows.push(["Năm sinh vợ /chồng: ", "----", ""]);	
					}	
				}
				else
				{
					rows.push(["Năm kết hôn: ", "----", ""]);
					rows.push(["Năm sinh vợ /chồng: ", "----", ""]);
				}

				if(nam[ttNam].thangam == 0)
				{
					rows.push(["TS-ĐS: ", "Vui lòng nhập đủ ngày tháng sanh!",""]);
					createTableRows(rows);
				}
				else
				{
					$.post("/batdauxem/tsds",
					{
						nguHanh: timNguHanh(nam[ttNam].canam, nam[ttNam].chiam),
						thang: nam[ttNam].thangam
					}
					,function(data){
						rows.push(["TS-ĐS: ", data.tsds, ""]);
						createTableRows(rows);
					});
				}
			});				
		}
	}

	function showAllInfo(nam, nu, namxem, luachon)
	{
		var bang =$('#bangchitiet'); 
		bang.empty();
		$("#tieudeXemVe").text("");
		$("#tieudeNu").text("");
		$("#tieudeNam").text("");
		$("#warning").hide();
		var ttNam=Number($("#hoten-namsinh-nam").val());
		var ttNu=Number($("#hoten-namsinh-nu").val());

		var namxem=Number($("#namxem").val());
		var valNam ,valNu;
		var canChiNamXem=getYearCanChi1(namxem);

		//khi chọn cơ bản
		if(luachon=="1")
		{
			//khi nữ có chọn và nam không chọn
			if(ttNam =="-1" && ttNu !="-1")
			{
				getFeMaleInfo(nu, ttNu, namxem);				
			}
			//khi nam có chọn và nữ không chọn
			else if(ttNam !="-1" && ttNu =="-1")
			{
				getMaleInfo(nam, ttNam, namxem);		
			}
			//khi chọn cả 2
			else if(ttNam !="-1" && ttNu !="-1")
			{
				//khi năm sinh âm lớn hơn năm xem
				if(nam[ttNam].namam > namxem || nu[ttNu].namam > namxem)
				{
					alert("Năm xem đang nhỏ hơn Năm sinh. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Cơ bản");
				}
				//khi khoảng cách giữa năm sinh âm và năm xem lớn hơn 120 năm
				else if((namxem-nam[ttNam].namam +1) >120 || (namxem-nu[ttNu].namam +1) >120)
				{
					alert("Khoảng cách giữa Năm xem và Năm sinh lớn hơn 120 năm. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Cơ bản");
				}
				else 
				{
					$("#tieudeXemVe").text("");
					$("#tieudeNu").text("NỮ");
					$("#tieudeNam").text("NAM");

					$.post("/ban-than/getDetail",{
					year: nam[ttNam].namam
					} 
					,function(data2){
						$.post("/ban-than/getDetail",{
						year: nu[ttNu].namam
						} 
						,function(data){
							$.post("/batdauxem/ketHopCungPhi",
							{	
								cungPhiNam: CUNGPHINAM[nam[ttNam].namam % 9],
								cungPhiNu: CUNGPHINU[nu[ttNu].namam % 9]
							}
							,function(data1){
								$.post("/batdauxem/ketHopCungPhi",
								{	
									cungPhiNam: data2.cungsanh,
									cungPhiNu: data.cungsanh
								}
								,function(data3){
									var rows = [
										["Tên:", nam[ttNam].ten, nu[ttNu].ten],
										["Tuổi:",namxem - nam[ttNam].namam +1, namxem - nu[ttNu].namam + 1],
										["Con thứ:", nam[ttNam].conthu, nu[ttNu].conthu],
										["Năm:",nam[ttNam].canam+" "+nam[ttNam].chiam, nu[ttNu].canam+" "+nu[ttNu].chiam],
										["Mạng:", data2.mang + " (" + data2.ynm + ")", data.mang + " (" + data.ynm + ")"],
										["Mạng khắc:", data2.mangkhac, data.mangkhac],
										['Cung sanh:<a href="#" value="" class="btn-kethop" id="btnKetHop" data-toggle="modal" data-target="#ynghiakethopcungsanh" data-whatever="@mdo"><span style="margin-left:2px; text-shadow: 2px 2px 10px gold">+</span></a>', data2.cungsanh, data.cungsanh],
										['Cung phi:<a href="#" value="" class="btn-kethop" id="btnKetHop" data-toggle="modal" data-target="#ynghiakethop" data-whatever="@mdo"><span style="margin-left:2px; text-shadow: 2px 2px 10px gold">+</span></a>', CUNGPHINAM[(nam[ttNam].namam%9)], CUNGPHINU[(nu[ttNu].namam%9)]],
										["Mệnh phong thủy:", MENHPHONGTHUYNAM[(nam[ttNam].namam%9)], MENHPHONGTHUYNU[(nu[ttNu].namam%9)]],
										["Con nhà:", data2.connha + " (" + data2.yncn + ")", data.connha + " (" + data.yncn + ")"],
										["Xương:", data2.xuong , data.xuong],
										["Tướng tinh:", data2.tuongtinh , data.tuongtinh],
										["Độ mạng:", data2.domangnam , data.domangnu]
									];

									//kiểm tra nếu có năm kết hôn thì xuất ra
									if(nam[ttNam].namkethon!=0 && nu[ttNu].namkethon==0)
									{
										rows.push(["Năm kết hôn: ", nam[ttNam].namkethon, "----"]);
										if(nam[ttNam].namsinhdoiphuong !=0)
										{
											rows.push(["Năm sinh vợ /chồng: ", nam[ttNam].namsinhdoiphuong + " (" + (namxem - Number(nam[ttNam].namsinhdoiphuong) + 1) + "t)", "----"]);
										}
										else
										{
											rows.push(["Năm sinh vợ /chồng: ", "----", "----"]);
										}	
										
									}
									else if(nam[ttNam].namkethon == 0 && nu[ttNu].namkethon != 0)
									{
										rows.push(["Năm kết hôn: ", "----",nu[ttNu].namkethon]);
										if(nu[ttNu].namsinhdoiphuong != 0)
										{
											rows.push(["Năm sinh vợ /chồng: ", "----",nu[ttNu].namsinhdoiphuong + " (" + (namxem - Number(nu[ttNu].namsinhdoiphuong) + 1) + "t)"]);
										}
										else
										{
											rows.push(["Năm sinh vợ /chồng: ", "----","----"]);
										}								
									}
									else if(nam[ttNam].namkethon != 0 && nu[ttNu].namkethon != 0)
									{
										rows.push(["Năm kết hôn: ", nam[ttNam].namkethon, nu[ttNu].namkethon]);
										if(nam[ttNam].namsinhdoiphuong == 0 && nu[ttNu].namsinhdoiphuong != 0)
										{
											rows.push(["Năm sinh vợ /chồng: ", "----", nu[ttNu].namsinhdoiphuong + " (" + (namxem - Number(nu[ttNu].namsinhdoiphuong) + 1) + "t)"]);
										}
										else if(nam[ttNam].namsinhdoiphuong != 0 && nu[ttNu].namsinhdoiphuong == 0)
										{
											rows.push(["Năm sinh vợ /chồng: ", nam[ttNam].namsinhdoiphuong + " (" + (namxem - Number(nam[ttNam].namsinhdoiphuong) + 1) + "t)", "----"]);
										}
										else if(nam[ttNam].namsinhdoiphuong != 0 && nu[ttNu].namsinhdoiphuong != 0)
										{
											rows.push(["Năm sinh vợ /chồng: ", nam[ttNam].namsinhdoiphuong + " (" + (namxem - Number(nam[ttNam].namsinhdoiphuong) + 1) + "t)", nu[ttNu].namsinhdoiphuong + " (" + (namxem - Number(nu[ttNu].namsinhdoiphuong) + 1)+ "t)"]);
										}
										else
										{
											rows.push(["Năm sinh vợ /chồng: ", "----", "----"]);	
										}
									}
									else
									{
										rows.push(["Năm kết hôn: ", "----", "----"]);
										rows.push(["Năm sinh vợ /chồng: ", "----", "----"]);
									}

									//kiểm tra xem quyền phần cung sanh và cung phi
									if(data1==0 || data3==0)
									{
										$("#tieudekethop").text("");
										$("#noidungynghia").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
										$("#tieudekethopcungsanh").text("");
										$("#noidungynghiacungsanh").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem,, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
									}
									else
									{
										$("#tieudekethop").text(data1.kethop);
										$("#noidungynghia").text("Ý nghĩa: "+data1.ynkethop);
										$("#tieudekethopcungsanh").text(data3.kethop);
										$("#noidungynghiacungsanh").text("Ý nghĩa: "+data3.ynkethop);
									}

									if(nam[ttNam].thangam == 0 && nu[ttNu].thangam != 0)
									{
										$.post("/batdauxem/tsds",
										{
											nguHanh: timNguHanh(nu[ttNu].canam, nu[ttNu].chiam),
											thang: nu[ttNu].thangam
										}
										,function(data4){
											rows.push(["TS-ĐS: ", "Vui lòng nhập đủ ngày tháng sanh!", data4.tsds]);
											createTableRows(rows);
										});
									}
									else if(nam[ttNam].thangam != 0 && nu[ttNu].thangam == 0)
									{
										$.post("/batdauxem/tsds",
										{
											nguHanh: timNguHanh(nam[ttNam].canam, nam[ttNam].chiam),
											thang: nam[ttNam].thangam
										}
										,function(data4){
											rows.push(["TS-ĐS: ", data4.tsds, "Vui lòng nhập đủ ngày tháng sanh!"]);
											createTableRows(rows);
										});
									}
									else if(nam[ttNam].thangam != 0 && nu[ttNu].thangam != 0)
									{
										$.post("/batdauxem/tsds",
										{
											nguHanh: timNguHanh(nu[ttNu].canam, nu[ttNu].chiam),
											thang: nu[ttNu].thangam
										}
										,function(data4){
											$.post("/batdauxem/tsds",
											{
												nguHanh: timNguHanh(nam[ttNam].canam, nam[ttNam].chiam),
												thang: nam[ttNam].thangam
											}
											,function(data5){
												rows.push(["TS-ĐS: ", data5.tsds, data4.tsds]);
												createTableRows(rows);
											});
										});
									}
									else
									{
										rows.push(["TS-ĐS: ", "Vui lòng nhập đủ ngày tháng sanh!", "Vui lòng nhập đủ ngày tháng sanh!"]);
										createTableRows(rows);
									}
								});
							});
						});
					});
				}
			}
			//khi không chọn cả 2
			else 
			{
				alert("Vui lòng chọn 1 đối tượng để xem");
				//$("#xemve").val("0");
				//$("#xemve option:selected").text("Cơ bản");
			}
		}
		//chọn phần xem tốt/xấu trong năm
		else if(luachon=="2")
		{
			var rows=[];
			//khi nữ có chọn và nam không chọn
			if(ttNam =="-1" && ttNu !="-1")
			{
				//khi năm sinh âm lớn hơn năm xem
				if(nu[ttNu].namam > namxem)
				{
					alert("Năm xem đang nhỏ hơn Năm sinh. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Cơ bản");
				}
				//khi khoảng cách giữa năm sinh âm và năm xem lớn hơn 120 năm
				else if((namxem - nu[ttNu].namam + 1) >120)
				{
					alert("Khoảng cách giữa Năm xem và Năm sinh lớn hơn 120 năm. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Cơ bản");
				}
				else
				{
					$("#tieudeXemVe").text("");
					$("#tieudeNu").text("NỮ");
					$("#tieudeNam").text("");

					$.post("/batdauxem/layNienVan",{
					tuoi: namxem - nu[ttNu].namam +1
					},function(data){
						var display="none"

						if(data.gtsaonam=="0")
						{
							$(".thongbao").show();
							$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
							$("#giainghiasaonam").text("");
							$("#giainghiasaonu").text("");
							$("#giainghiahannam").text("");
							$("#giainghiahannu").text("");
							$("#giainghiavanniennam").text("");
							$("#giainghiavanniennu").text("");
						}
						else
						{
							$("#btnKetHop").show();
							$(".thongbao").hide();
							$("#giainghiasaonam").text("");
							$("#giainghiasaonu").text(data.saonam + ": " + data.gtsaonam);
							$("#giainghiahannam").text("");
							$("#giainghiahannu").text(data.hannam + ": " + data.gthannam);
							$("#giainghiavanniennam").text("");
							$("#giainghiavanniennu").text(data.vannien + ": " + data.gtvannien);
							display="show";
						}

						rows = [
							['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiasao" data-whatever="@mdo">Sao:<span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", data.saonam + "<br> (" +data.ynsaonam+")"],
							['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahan" data-whatever="@mdo">Hạn: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", data.hannam + "<br> (" +data.ynhannam+")"],
							['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiavannien" data-whatever="@mdo">Vận niên: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", data.vannien + "<br> (" +data.ynvannien+")"]
						];

						createTableRows(rows);

						if(data.kimlau!=null)
						{
							rows = rows.concat([
								['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiakimlau" data-whatever="@mdo">Kim lâu: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "",data.kimlau],
								['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahoangoc" data-whatever="@mdo">Hoang ốc: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "",data.hoangoc]
							]);

							if(data.gtkimlau=="0")
							{
								$(".thongbao").show();
								$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
								$("#giainghiakimlaunam").text("");
								$("#giainghiakimlaunu").text("");
								$("#giainghiahoangocnam").text("");
								$("#giainghiahoangocnu").text("");
							}
							else
							{
								$(".thongbao").hide();
								$("#giainghiakimlaunam").text("");
								$("#giainghiakimlaunu").text(data.kimlau + ": " + data.gtkimlau);
								$("#giainghiahoangocnam").text("");
								$("#giainghiahoangocnu").text(data.hoangoc + ": " + data.gthoangoc);
							}


							bang.empty();
							createTableRows(rows);						
						}
						else
						{
							rows = rows.concat([
								['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahoangoc" data-whatever="@mdo">Hoang ốc: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", data.hoangoc]
							]);

							if(data.gthoangoc=="0")
							{
								$(".thongbao").show();
								$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
								
								$("#giainghiahoangocnam").text("");
								$("#giainghiahoangocnu").text("");
							}
							else
							{
								$(".thongbao").hide();
								$("#giainghiahoangocnam").text("");
								$("#giainghiahoangocnu").text(data.hoangoc + ": " + data.gthoangoc);
							}

							bang.empty();
							createTableRows(rows);
						}

						$.post("/batdauxem/layThaiTue",{
						tuoi: nu[ttNu].chiam,
						namHienTai: canChiNamXem[1]
						} 
						,function(ThaiTuedata){
							if(ThaiTuedata!=0)
							{
								if(ThaiTuedata.hthaitue==null)
								{
									rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "" , ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")"]);
	
									if(ThaiTuedata.gtxungthaitue=="0")
									{
										$(".thongbao").show();
										$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
										$("#giainghiathaituenam").text("");
										$("#giainghiathaituenu").text("");
									}
									else
									{
										$("#giainghiathaituenam").text("");
										$("#giainghiathaituenu").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
									}
								}
								else
								{
									rows = rows.concat([
										['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "",ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")"],
										['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahinhthaitue" data-whatever="@mdo">Hình thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "",ThaiTuedata.hthaitue + "<br> (" + ThaiTuedata.ynhthaitue + ")"]
									]);	

									if(ThaiTuedata.gtxungthaitue=="0")
									{
										$(".thongbao").show();
										$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
										$("#giainghiathaituenam").text("");
										$("#giainghiathaituenu").text("");
										$("#giainghiahinhthaituenam").text("");
										$("#giainghiahinhthaituenu").text("");
									}
									else
									{
										$("#giainghiathaituenam").text("");
										$("#giainghiathaituenu").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
										$("#giainghiahinhthaituenam").text("");
										$("#giainghiahinhthaituenu").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
									}												
								}
								bang.empty();
								createTableRows(rows);	
							}

							$.post("/batdauxem/layTamTai",{
							tuoi: nu[ttNu].chiam,
							namHienTai: canChiNamXem[1]
							} 
							,function(TamTaidata){
								if(TamTaidata !="0")
								{
									rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiatamtai" data-whatever="@mdo">Tam tai: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "",TamTaidata.tamtai + "<br> (" + TamTaidata.yntt + ")"]);
									

									if(TamTaidata.gttamtai=="0")
									{
										$(".thongbao").show();
										$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
										$("#giainghiatamtainam").text("");
										$("#giainghiatamtainu").text("");
									}
									else
									{
										$("#giainghiatamtainam").text("");
										$("#giainghiatamtainu").text(TamTaidata.tamtai + ": " + TamTaidata.gttamtai);
									}

									bang.empty();
									createTableRows(rows);	
								}
							});
							$.post("/batdauxem/layKoCuoiHoi",{
								gioiTinh : nu[ttNu].gioitinh,
								tuoi: nu[ttNu].chiam,
								namHienTai: canChiNamXem[1]
							} 
							,function(CuoiHoidata){
								if(CuoiHoidata !="0")
								{
									rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiacuoihoi" data-whatever="@mdo">Cưới hỏi: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", CuoiHoidata.cuoihoi]);

									if(CuoiHoidata.gtcuoihoi=="0")
									{
										$(".thongbao").show();
										$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
										$("#giainghiacuoihoinam").text("");
										$("#giainghiacuoihoinu").text("");
									}
									else
									{
										$("#giainghiacuoihoinam").text("");
										$("#giainghiacuoihoinu").text(CuoiHoidata.cuoihoi + ": " + CuoiHoidata.gtcuoihoi);
									}
									bang.empty();
									createTableRows(rows);
								}
							});

							$.post('/batdauxem/layThoiThe',
							{
								canThanChu: nu[ttNu].canam,
								canNamHienTai: canChiNamXem[0]

							},function (thoiTheData) {
								if(thoiTheData!=0)
								{
									rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop">Thời thế:</a>', "",thoiTheData.ketqua]);
									bang.empty();
									createTableRows(rows);
								}
							});	
						});			
					});
				}
			}
			//khi nam có chọn và nữ không chọn
			else if(ttNam !="-1" && ttNu =="-1")
			{
				//khi năm sinh âm lớn hơn năm xem
				if(nam[ttNam].namam > namxem )
				{
					alert("Năm xem đang nhỏ hơn Năm sinh. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Cơ bản");
				}
				//khi khoảng cách giữa năm sinh âm và năm xem lớn hơn 120 năm
				else if((namxem-nam[ttNam].namam +1) >120)
				{
					alert("Khoảng cách giữa Năm xem và Năm sinh lớn hơn 120 năm. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Cơ bản");
				}
				else
				{
					$("#tieudeXemVe").text("");
					$("#tieudeNu").text("");
					$("#tieudeNam").text("NAM");

					$.post("/batdauxem/layNienVan",{
					tuoi: namxem - nam[ttNam].namam +1
					},function(data){
						var display="none";

						if(data.gtsaonam=="0")
						{
							$(".thongbao").show();
							$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
							$("#giainghiasaonam").text("");
							$("#giainghiasaonu").text("");
							$("#giainghiahannam").text("");
							$("#giainghiahannu").text("");
							$("#giainghiavanniennam").text("");
							$("#giainghiavanniennu").text("");
						}
						else
						{
							$(".thongbao").hide();
							$("#giainghiasaonam").text(data.saonam + ": " + data.gtsaonam);
							$("#giainghiasaonu").text("");
							$("#giainghiahannam").text(data.hannam + ": " + data.gthannam);
							$("#giainghiahannu").text("");
							$("#giainghiavanniennam").text(data.vannien + ": " + data.gtvannien);
							$("#giainghiavanniennu").text("");
							display="show"
						}



						rows = [
							['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiasao" data-whatever="@mdo">Sao: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data.saonam + "<br> (" +data.ynsaonam+")", ""],
							['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahan" data-whatever="@mdo">Hạn: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>',data.hannam + "<br> (" +data.ynhannam+")", ""],
							['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiavannien" data-whatever="@mdo">Vận niên: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>',data.vannien + "<br> (" +data.ynvannien+")", ""]
						];

						createTableRows(rows);
						if(data.kimlau!=null)
						{
							rows = rows.concat([
								['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiakimlau" data-whatever="@mdo">Kim lâu: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data.kimlau, ""],
								['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahoangoc" data-whatever="@mdo">Hoang ốc: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data.hoangoc, ""]
							]);

							if(data.gtkimlau=="0")
							{
								$(".thongbao").show();
								$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
								$("#giainghiakimlaunam").text("");
								$("#giainghiakimlaunu").text("");
								$("#giainghiahoangocnam").text("");
								$("#giainghiahoangocnu").text("");
							}
							else
							{
								$(".thongbao").hide();
								$("#giainghiakimlaunam").text(data.kimlau + ": " + data.gtkimlau);
								$("#giainghiakimlaunu").text("");
								$("#giainghiahoangocnam").text(data.hoangoc + ": " + data.gthoangoc);
								$("#giainghiahoangocnu").text("");
							}

							bang.empty();
							createTableRows(rows);						
						}
						else
						{
							rows = rows.concat([
								['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahoangoc" data-whatever="@mdo">Hoang ốc: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data.hoangoc, ""]
							]);

							if(data.gthoangoc=="0")
							{
								$(".thongbao").show();
								$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
								$("#giainghiahoangocnam").text("");
								$("#giainghiahoangocnu").text("");
							}
							else
							{
								$(".thongbao").hide();;
								$("#giainghiahoangocnam").text(data.hoangoc + ": " + data.gthoangoc);
								$("#giainghiahoangocnu").text("");
							}

							bang.empty();
							createTableRows(rows);
						}

						$.post("/batdauxem/layThaiTue",{
						tuoi: nam[ttNam].chiam,
						namHienTai: canChiNamXem[1]
						} 
						,function(ThaiTuedata){
							if(ThaiTuedata!=0)
							{
								if(ThaiTuedata.hthaitue==null)
								{
									rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")", ""]);
									
									if(ThaiTuedata.gtxungthaitue=="0")
									{
										$(".thongbao").show();
										$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
										$("#giainghiathaituenam").text("");
										$("#giainghiathaituenu").text("");
									}
									else
									{
										$("#giainghiathaituenam").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
										$("#giainghiathaituenu").text("");
									}

									bang.empty();
									createTableRows(rows);
								}
								else
								{
									rows = rows.concat([
										['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")", ""],
										['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahinhthaitue" data-whatever="@mdo">Hình thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.hthaitue + "<br> (" + ThaiTuedata.ynhthaitue + ")", ""]
									]);	

									if(ThaiTuedata.gtxungthaitue=="0")
									{
										$(".thongbao").show();
										$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
										$("#giainghiathaituenam").text("");
										$("#giainghiathaituenu").text("");
										$("#giainghiahinhthaituenam").text("");
										$("#giainghiahinhthaituenu").text("");
									}
									else
									{
										$("#giainghiathaituenam").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
										$("#giainghiathaituenu").text("");
										$("#giainghiahinhthaituenam").text(ThaiTuedata.hthaitue + ": " + ThaiTuedata.gthinhthaitue);
										$("#giainghiahinhthaituenu").text("");
									}

									bang.empty();
									createTableRows(rows);						
								}
							}

							$.post("/batdauxem/layTamTai",{
							tuoi: nam[ttNam].chiam,
							namHienTai: canChiNamXem[1]
							} 
							,function(TamTaidata){
								if(TamTaidata !="0")
								{		
									rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiatamtai" data-whatever="@mdo">Tam tai: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', TamTaidata.tamtai + "<br> (" + TamTaidata.yntt + ")" ,""]);
									bang.empty();

									if(TamTaidata.gttamtai=="0")
									{
										$(".thongbao").show();
										$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
										$("#giainghiatamtainam").text("");
										$("#giainghiatamtainu").text("");
									}
									else
									{
										$("#giainghiatamtainam").text(TamTaidata.tamtai + ": " + TamTaidata.gttamtai);
										$("#giainghiatamtainu").text("");
									}

									createTableRows(rows);	
								}
							});
							$.post("/batdauxem/layKoCuoiHoi",{
								gioiTinh : nam[ttNam].gioitinh,
								tuoi: nam[ttNam].chiam,
								namHienTai: canChiNamXem[1]
							} 
							,function(CuoiHoidata){
								if(CuoiHoidata !="0")
								{
									rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiacuoihoi" data-whatever="@mdo">Cưới hỏi: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', CuoiHoidata.cuoihoi, ""]);
									
									if(CuoiHoidata.gtcuoihoi=="0")
									{
										$(".thongbao").show();
										$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
										$("#giainghiacuoihoinam").text("");
										$("#giainghiacuoihoinu").text("");
									}
									else
									{
										$("#giainghiacuoihoinam").text(CuoiHoidata.cuoihoi + ": " + CuoiHoidata.gtcuoihoi);
										$("#giainghiacuoihoinu").text("");
									}

									bang.empty();
									createTableRows(rows);
								}
								//$("#xemve").val("0");
								//$("#xemve option:selected").text("Tốt/xấu trong năm");
							});	
							$.post('/batdauxem/layThoiThe',
							{
								canThanChu: nam[ttNam].canam,
								canNamHienTai: canChiNamXem[0]

							},function (thoiTheData) {
								if(thoiTheData!=0)
								{
									rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop">Thời thế:</a>', thoiTheData.ketqua,""]);
									bang.empty();
									createTableRows(rows);
								}
							});	
						});			
					});
				}
			}
			//khi cả 2 đều chọn
			else if(ttNam !="-1" && ttNu !="-1")
			{
				//khi năm sinh âm lớn hơn năm xem
				if(nam[ttNam].namam > namxem || nu[ttNu].namam > namxem)
				{
					alert("Năm xem đang nhỏ hơn Năm sinh. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Tốt/xấu trong năm");
				}
				//khi khoảng cách giữa năm sinh âm và năm xem lớn hơn 120 năm
				else if((namxem-nam[ttNam].namam +1) >120 || (namxem-nu[ttNu].namam +1) >120)
				{
					alert("Khoảng cách giữa Năm xem và Năm sinh lớn hơn 120 năm. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Tốt/xấu trong năm");
				}
				else
				{
					$("#tieudeXemVe").text("");
					$("#tieudeNu").text("NỮ");
					$("#tieudeNam").text("NAM");
					var display = "none"; 

					$.post("/batdauxem/layNienVan",{
					tuoi: namxem - nam[ttNam].namam +1
					} 
					,function(data2){
						$.post("/batdauxem/layNienVan",{
						tuoi: namxem - nu[ttNu].namam +1
						} 
						,function(data){
							if(data2.gtsaonam=="0")
							{
								$(".thongbao").show();
								$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
								$("#giainghiasaonam").text("");
								$("#giainghiasaonu").text("");
								$("#giainghiahannam").text("");
								$("#giainghiahannu").text("");
								$("#giainghiavanniennam").text("");
								$("#giainghiavanniennu").text("");
							}
							else
							{
								$(".thongbao").hide();
								$("#giainghiasaonam").text(data2.saonam + ": " + data2.gtsaonam);
								$("#giainghiasaonu").text(data.saonu + ": " + data.gtsaonu);
								$("#giainghiahannam").text(data2.hannam + ": " + data2.gthannam);
								$("#giainghiahannu").text(data.hannu + ": " + data.gthannu);
								$("#giainghiavanniennam").text(data2.vannien + ": " + data2.gtvannien);
								$("#giainghiavanniennu").text(data.vannien + ": " + data.gtvannien);
								display="show";
							}


							rows = [
								['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiasao" data-whatever="@mdo">Sao: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data2.saonam + "<br> (" +data2.ynsaonam+")", data.saonu + "<br> ("+ data.ynsaonu+")"],
								['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahan" data-whatever="@mdo">Hạn: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>',data2.hannam + "<br> (" +data2.ynhannam+")", data.hannu + "<br> ("+ data.ynhannu+")"],
								['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiavannien" data-whatever="@mdo">Vận niên: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a> ',data2.vannien + "<br> (" +data2.ynvannien+")", data.vannien + "<br> ("+ data.ynvannien+")"]
							];

							//alert(data2.gtsaonam);
							createTableRows(rows);
							

							if(data2.kimlau==null && data.kimlau!=null)
							{
								rows = rows.concat([
									['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiakimlau" data-whatever="@mdo">Kim lâu: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", data.kimlau],
									['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahoangoc" data-whatever="@mdo">Hoang ốc: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data2.hoangoc, data.hoangoc]
								]);

								if(data.gtkimlau=="0")
								{
									$(".thongbao").show();
									$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
									$("#giainghiakimlaunam").text("");
									$("#giainghiakimlaunu").text("");
									$("#giainghiahoangocnam").text("");
									$("#giainghiahoangocnu").text("");
								}
								else
								{
									$(".thongbao").hide();
									$("#giainghiakimlaunam").text("");
									$("#giainghiakimlaunu").text(data.kimlau + ": " + data.gtkimlau);
									$("#giainghiahoangocnam").text(data2.hoangoc + ": " + data2.gthoangoc);
									$("#giainghiahoangocnu").text(data.hoangoc + ": " + data.gthoangoc);
								}

								bang.empty();
								createTableRows(rows);
													
							}
							else if(data2.kimlau!=null && data.kimlau==null)
							{
								rows = rows.concat([
									['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiakimlau" data-whatever="@mdo">Kim lâu: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data2.kimlau, ""],
									['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahoangoc" data-whatever="@mdo">Hoang ốc: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data2.hoangoc, data.hoangoc]
								]);

								if(data2.gtkimlau=="0")
								{
									$(".thongbao").show();
									$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
									$("#giainghiakimlaunam").text("");
									$("#giainghiakimlaunu").text("");
									$("#giainghiahoangocnam").text("");
									$("#giainghiahoangocnu").text("");
								}
								else
								{
									$(".thongbao").hide();
									$("#giainghiakimlaunam").text(data2.kimlau + ": " + data2.gtkimlau);
									$("#giainghiakimlaunu").text("");
									$("#giainghiahoangocnam").text(data2.hoangoc + ": " + data2.gthoangoc);
									$("#giainghiahoangocnu").text(data.hoangoc + ": " + data.gthoangoc);
								}

								bang.empty();
								createTableRows(rows);	
							}
							else if(data2.kimlau!=null && data.kimlau!=null)
							{
								rows = rows.concat([
									['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiakimlau" data-whatever="@mdo">Kim lâu: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data2.kimlau, data.kimlau],
									['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahoangoc" data-whatever="@mdo">Hoang ốc: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data2.hoangoc, data.hoangoc]
								]);

								if(data2.gtkimlau=="0")
								{
									$(".thongbao").show();
									$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
									$("#giainghiakimlaunam").text("");
									$("#giainghiakimlaunu").text("");
									$("#giainghiahoangocnam").text("");
									$("#giainghiahoangocnu").text("");
								}
								else
								{
									$(".thongbao").hide();
									$("#giainghiakimlaunam").text(data2.kimlau + ": " + data2.gtkimlau);
									$("#giainghiakimlaunu").text(data.kimlau + ": " + data.gtkimlau);
									$("#giainghiahoangocnam").text(data2.hoangoc + ": " + data2.gthoangoc);
									$("#giainghiahoangocnu").text(data.hoangoc + ": " + data.gthoangoc);
								}

								bang.empty();
								createTableRows(rows);
							}
							else
							{
								rows = rows.concat([
									['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahoangoc" data-whatever="@mdo">Hoang ốc: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', data2.hoangoc, data.hoangoc]
								]);

								if(data2.gthoangoc=="0")
								{
									$(".thongbao").show();
									$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
									$("#giainghiahoangocnam").text("");
									$("#giainghiahoangocnu").text("");
								}
								else
								{
									$(".thongbao").hide();
									$("#giainghiahoangocnam").text(data2.hoangoc + ": " + data2.gthoangoc);
									$("#giainghiahoangocnu").text(data.hoangoc + ": " + data.gthoangoc);
								}

								bang.empty();
								createTableRows(rows);				
							}

					});
					//lấy thái tuế
					$.post("/batdauxem/layThaiTue",{
							tuoi: nam[ttNam].chiam,
							namHienTai: canChiNamXem[1]
						} 
						,function(ThaiTuedata){
							$.post("/batdauxem/layThaiTue",{
								tuoi: nu[ttNu].chiam,
								namHienTai: canChiNamXem[1]
								} 
							,function(ThaiTuedata1){
								if(ThaiTuedata =="0")
								{
									if(ThaiTuedata1!="0")
									{
										if(ThaiTuedata1.hthaitue==null)
										{
											if(ThaiTuedata1.gtxungthaitue=="0")
											{
												$(".thongbao").show();
												$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
												$("#giainghiathaituenam").text("");
												$("#giainghiathaituenu").text("");
											}
											else
											{
												$("#giainghiathaituenam").text("");
												$("#giainghiathaituenu").text(ThaiTuedata1.xthaitue + ": " + ThaiTuedata1.gtxungthaitue);
											}

											rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", ThaiTuedata1.xthaitue + "<br> (" + ThaiTuedata1.ynxungthaitue + ")"]);
											
											bang.empty();
											createTableRows(rows);
										}
										else
										{
											rows = rows.concat([
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", ThaiTuedata1.xthaitue + "<br> (" + ThaiTuedata1.ynxungthaitue + ")"],
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahinhthaitue" data-whatever="@mdo">Hình thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", ThaiTuedata1.hthaitue + "<br> (" + ThaiTuedata1.ynhthaitue + ")"]
											]);

											if(ThaiTuedata1.gtxungthaitue=="0")
											{
												$(".thongbao").show();
												$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
												$("#giainghiathaituenam").text("");
												$("#giainghiathaituenu").text("");
												$("#giainghiahinhthaituenam").text("");
												$("#giainghiahinhthaituenu").text("");
											}
											else
											{
												$("#giainghiathaituenam").text("");
												$("#giainghiathaituenu").text(ThaiTuedata1.xthaitue + ": " + ThaiTuedata1.gtxungthaitue);
												$("#giainghiahinhthaituenam").text("");
												$("#giainghiahinhthaituenu").text(ThaiTuedata1.hthaitue + ": " + ThaiTuedata1.gthinhthaitue);
											}

											bang.empty();
											createTableRows(rows);					
										}
									}
								}
								else
								{
									if(ThaiTuedata1=="0")
									{
										if(ThaiTuedata.hthaitue==null)
										{
											rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")", ""]);
											
											if(ThaiTuedata.gtxungthaitue=="0")
											{
												$(".thongbao").show();
												$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
												$("#giainghiathaituenam").text("");
												$("#giainghiathaituenu").text("");
											}
											else
											{
												$("#giainghiathaituenam").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
												$("#giainghiathaituenu").text("");
											}
											bang.empty();
											createTableRows(rows);
										}
										else
										{

											rows = rows.concat([
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")", ""],
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahinhthaitue" data-whatever="@mdo">Hình thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.hthaitue + "<br> (" + ThaiTuedata.ynhthaitue + ")", ""]
											]);

											if(ThaiTuedata.gtxungthaitue=="0")
											{
												$(".thongbao").show();
												$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
												$("#giainghiathaituenam").text("");
												$("#giainghiathaituenu").text("");
												$("#giainghiahinhthaituenam").text("");
												$("#giainghiahinhthaituenu").text("");
											}
											else
											{
												$("#giainghiathaituenam").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
												$("#giainghiathaituenu").text("");
												$("#giainghiahinhthaituenam").text(ThaiTuedata.hthaitue + ": " + ThaiTuedata.gthinhthaitue);
												$("#giainghiahinhthaituenu").text("");
											}

											bang.empty();
											createTableRows(rows);
										}
									}
									else
									{
										if(ThaiTuedata.hthaitue==null)
										{
											if(ThaiTuedata1.hthaitue==null)
											{
												rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")", ThaiTuedata1.xthaitue + "<br> (" + ThaiTuedata1.ynxungthaitue + ")"]);
												
												if(ThaiTuedata.gtxungthaitue=="0")
												{
													$(".thongbao").show();
													$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
													$("#giainghiathaituenam").text("");
													$("#giainghiathaituenu").text("");
												}
												else
												{
													$("#giainghiathaituenam").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
													$("#giainghiathaituenu").text(ThaiTuedata1.xthaitue + ": " + ThaiTuedata1.gtxungthaitue);
												}

												bang.empty();
												createTableRows(rows);
											}
											else
											{
												rows = rows.concat([
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")", ThaiTuedata1.xthaitue + "<br> (" + ThaiTuedata1.ynxungthaitue + ")"],
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahinhthaitue" data-whatever="@mdo">Hình thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", ThaiTuedata1.hthaitue + "\n (" + ThaiTuedata1.ynhthaitue + ")"]
												]);

												if(ThaiTuedata.gtxungthaitue=="0")
												{
													$(".thongbao").show();
													$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
													$("#giainghiathaituenam").text("");
													$("#giainghiathaituenu").text("");
													$("#giainghiahinhthaituenam").text("");
													$("#giainghiahinhthaituenu").text("");
												}
												else
												{
													$("#giainghiathaituenam").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
													$("#giainghiathaituenu").text(ThaiTuedata1.xthaitue + ": " + ThaiTuedata1.gtxungthaitue);
													$("#giainghiahinhthaituenam").text("");
													$("#giainghiahinhthaituenu").text(ThaiTuedata1.hthaitue + ": " + ThaiTuedata1.gthinhthaitue);
												}

												bang.empty();
												createTableRows(rows);
											}
											
										}
										else
										{
											if(ThaiTuedata1.hthaitue==null)
											{
												rows = rows.concat([
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")", ThaiTuedata1.xthaitue + "<br> (" + ThaiTuedata1.ynxungthaitue + ")"],
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahinhthaitue" data-whatever="@mdo">Hình thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.hthaitue + "<br> (" + ThaiTuedata.ynhthaitue + ")", ""]
												]);


												if(ThaiTuedata.gtxungthaitue=="0")
												{
													$(".thongbao").show();
													$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
													$("#giainghiathaituenam").text("");
													$("#giainghiathaituenu").text("");
													$("#giainghiahinhthaituenam").text("");
													$("#giainghiahinhthaituenu").text("");
												}
												else
												{
													$("#giainghiathaituenam").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
													$("#giainghiathaituenu").text(ThaiTuedata1.xthaitue + ": " + ThaiTuedata1.gtxungthaitue);
													$("#giainghiahinhthaituenam").text(ThaiTuedata.hthaitue + ": " + ThaiTuedata.gthinhthaitue);
													$("#giainghiahinhthaituenu").text("");
												}

												bang.empty();
												createTableRows(rows);
											}
											else
											{
												rows = rows.concat([
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiathaitue" data-whatever="@mdo">Thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.xthaitue + "<br> (" + ThaiTuedata.ynxungthaitue + ")", ThaiTuedata1.xthaitue + "<br> (" + ThaiTuedata1.ynxungthaitue + ")"],
												['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiahinhthaitue" data-whatever="@mdo">Hình thái tuế: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', ThaiTuedata.hthaitue + "<br> (" + ThaiTuedata.ynhthaitue + ")", ThaiTuedata1.hthaitue + "<br> (" + ThaiTuedata1.ynhthaitue + ")"]
												]);

												if(ThaiTuedata.gtxungthaitue=="0")
												{
													$(".thongbao").show();
													$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
													$("#giainghiathaituenam").text("");
													$("#giainghiathaituenu").text("");
													$("#giainghiahinhthaituenam").text("");
													$("#giainghiahinhthaituenu").text("");
												}
												else
												{
													$("#giainghiathaituenam").text(ThaiTuedata.xthaitue + ": " + ThaiTuedata.gtxungthaitue);
													$("#giainghiathaituenu").text(ThaiTuedata1.xthaitue + ": " + ThaiTuedata1.gtxungthaitue);
													$("#giainghiahinhthaituenam").text(ThaiTuedata.hthaitue + ": " + ThaiTuedata.gthinhthaitue);
													$("#giainghiahinhthaituenu").text(ThaiTuedata1.hthaitue + ": " + ThaiTuedata1.gthinhthaitue);
												}

												bang.empty();
												createTableRows(rows);
											}										
										}
									}
								}			
							});					
						});
						//lấy tam tai bên nam
						$.post("/batdauxem/layTamTai",{
							tuoi: nam[ttNam].chiam,
							namHienTai: canChiNamXem[1]
						} 
						,function(TamTaidata){
							//lấy tam tai bên nữ
							$.post("/batdauxem/layTamTai",{
								tuoi: nu[ttNu].chiam,
								namHienTai: canChiNamXem[1]
								} 
							,function(TamTaidata1){
								//nếu bên nam không có
								if(TamTaidata =="0")
								{
									//nếu bên nữ có
									if(TamTaidata1!="0")
									{	
										rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiatamtai" data-whatever="@mdo">Tam tai: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", TamTaidata1.tamtai + "<br> (" + TamTaidata1.yntt + ")"]);
										
										if(TamTaidata1.gttamtai=="0")
										{
											$(".thongbao").show();
											$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
											$("#giainghiatamtainam").text("");
											$("#giainghiatamtainu").text("");
										}
										else
										{
											$("#giainghiatamtainam").text("");
											$("#giainghiatamtainu").text(TamTaidata1.tamtai + ": " + TamTaidata1.gttamtai);
										}

										bang.empty();
										createTableRows(rows);	
									}
								}
								//nếu có bên nam
								else
								{
									//nếu không có bên nữ
									if(TamTaidata1=="0")
									{		
										rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiatamtai" data-whatever="@mdo">Tam tai: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', TamTaidata.tamtai + " <br> (" + TamTaidata.yntt + ")", ""]);
										
										if(TamTaidata.gttamtai=="0")
										{
											$(".thongbao").show();
											$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
											$("#giainghiatamtainam").text("");
											$("#giainghiatamtainu").text("");
										}
										else
										{
											$("#giainghiatamtainam").text(TamTaidata.tamtai + ": " + TamTaidata.gttamtai);
											$("#giainghiatamtainu").text("");
										}

										bang.empty();
										createTableRows(rows);
									}
									//nếu có bên nữ
									else
									{	
										rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiatamtai" data-whatever="@mdo">Tam tai: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', TamTaidata.tamtai + "<br> (" + TamTaidata.yntt + ")", TamTaidata1.tamtai + "<br>  (" + TamTaidata1.yntt + ")"]);
										
										if(TamTaidata.gttamtai=="0")
										{
											$(".thongbao").show();
											$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
											$("#giainghiatamtainam").text("");
											$("#giainghiatamtainu").text("");
										}
										else
										{
											$("#giainghiatamtainam").text(TamTaidata.tamtai + ": " + TamTaidata.gttamtai);
											$("#giainghiatamtainu").text(TamTaidata1.tamtai + ": " + TamTaidata1.gttamtai);
										}

										bang.empty();
										createTableRows(rows);
									}
								}
							});
						});
						//lấy cưới hỏi bên nam
						$.post("/batdauxem/layKoCuoiHoi",{
							gioiTinh : nam[ttNam].gioitinh,
							tuoi: nam[ttNam].chiam,
							namHienTai: canChiNamXem[1]
						} 
						,function(CuoiHoidata){
							//lấy cưới hỏi bên nữ
							$.post("/batdauxem/layKoCuoiHoi",{
								gioiTinh : nu[ttNu].gioitinh,
								tuoi: nu[ttNu].chiam,
								namHienTai: canChiNamXem[1]
								} 
							,function(CuoiHoidata1){
								//nếu bên nam ko có
								if(CuoiHoidata =="0")
								 {
								 	//nếu bên nữ có
								 	if(CuoiHoidata1!="0")
									{		
										rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiacuoihoi" data-whatever="@mdo">Cưới hỏi: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', "", CuoiHoidata1.cuoihoi]);
										
										if(CuoiHoidata1.gtcuoihoi=="0")
										{
											$(".thongbao").show();
											$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
											$("#giainghiacuoihoinam").text("");
											$("#giainghiacuoihoinu").text("");
										}
										else
										{
											$("#giainghiacuoihoinam").text("");
											$("#giainghiacuoihoinu").text(CuoiHoidata1.cuoihoi + ": " + CuoiHoidata1.gtcuoihoi);
										}

										bang.empty();
										createTableRows(rows);
									}
								 }
								 else
								 {
								 	//nếu ko có thì thêm nam
								 	if(CuoiHoidata1=="0")
									{		
										rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiacuoihoi" data-whatever="@mdo">Cưới hỏi: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', CuoiHoidata.cuoihoi, ""]);
										
										if(CuoiHoidata.gtcuoihoi=="0")
										{
											$(".thongbao").show();
											$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
											$("#giainghiacuoihoinam").text("");
											$("#giainghiacuoihoinu").text("");
										}
										else
										{
											$("#giainghiacuoihoinam").text(CuoiHoidata.cuoihoi + ": " + CuoiHoidata.gtcuoihoi);
											$("#giainghiacuoihoinu").text("");
										}

										bang.empty();
										createTableRows(rows);
									}
									//nếu có thì thêm vào
									else
									{
										rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop" data-toggle="modal" data-target="#giainghiacuoihoi" data-whatever="@mdo">Cưới hỏi: <span style="margin-left:2px; text-shadow: 2px 2px 10px gold">!</span></a>', CuoiHoidata.cuoihoi, CuoiHoidata1.cuoihoi]);
										
										if(CuoiHoidata.gtcuoihoi=="0")
										{
											$(".thongbao").show();
											$(".thongbao").html("Hãy nâng cấp lên<b> Tài khoản VIP </b>để được xem, hoặc gửi bấm Gửi thông tin để được tư vấn miễn phí.");
											$("#giainghiacuoihoinam").text("");
											$("#giainghiacuoihoinu").text("");
										}
										else
										{
											$("#giainghiacuoihoinam").text(CuoiHoidata.cuoihoi + ": " + CuoiHoidata.gtcuoihoi);
											$("#giainghiacuoihoinu").text(CuoiHoidata1.cuoihoi + ": " + CuoiHoidata1.gtcuoihoi);
										}

										bang.empty();
										createTableRows(rows);			
									}
								 }
							});
						});	

						$.post('/batdauxem/layThoiThe',
						{
							canThanChu: nam[ttNam].canam,
							canNamHienTai: canChiNamXem[0]

						},function (thoiTheData) {
							$.post('/batdauxem/layThoiThe',
							{
								canThanChu: nu[ttNu].canam,
								canNamHienTai: canChiNamXem[0]

							},function (thoiTheData1) {
								if(thoiTheData!=0)
								{
									if(thoiTheData1!=0)
									{
										rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop">Thời thế:</span></a>', thoiTheData.ketqua, thoiTheData1.ketqua]);
										bang.empty();
										createTableRows(rows);
									}
									else
									{
										rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop">Thời thế:</a>', thoiTheData.ketqua, ""]);
										bang.empty();
										createTableRows(rows);
									}
								}
								else
								{
									if(thoiTheData1!=0)
									{
										rows.push(['<a style="display:'+ display +'" href="#" id="btnKetHop">Thời thế:</a>', "", thoiTheData1.ketqua]);
										bang.empty();
										createTableRows(rows);
									}
								}
							});	
						});	
					});		
				}
			}
			else
			{
				alert("Vui lòng chọn 1 đối tượng để xem");
				//$("#xemve").val("0");
				//$("#xemve option:selected").text("Tốt/xấu trong năm");
			}
		}
		//khi chọn vào hướng phong thủy
		else if(luachon == "5")
		{
			if(ttNam =="-1" && ttNu !="-1")
			{
				//khi năm sinh âm lớn hơn năm xem
				if(nu[ttNu].namam > namxem)
				{
					alert("Năm xem đang nhỏ hơn Năm sinh. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Hướng phong thủy");
				}
				//khi khoảng cách giữa năm sinh âm và năm xem lớn hơn 120 năm
				else if((namxem - nu[ttNu].namam + 1) >120)
				{
					alert("Khoảng cách giữa Năm xem và Năm sinh lớn hơn 120 năm. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Hướng phong thủy");
				}
				else
				{
					$("#tieudeXemVe").text("");
					$("#tieudeNu").text("NỮ");
					$("#tieudeNam").text("");
					var rows=[];

					$.post("/batdauxem/layHuongPhongThuy",{
					cungPhi: CUNGPHINU[nu[ttNu].namam % 9] 
					} 
					,function(data2){
						if(data2=="0")
						{
							$("#warning").show();
							rows = [
								["", "", ""]
							];

							createTableRows(rows);
						}
						else
						{
							rows = [
								["", "", "<b>" + CUNGPHINU[nu[ttNu].namam % 9] +"</b><br>"+ " (" + MENHPHONGTHUYNU[(nu[ttNu].namam%9)] + ")"],
								["Đông: ", "", data2.dong],
								["Tây: ", "", data2.tay],
								["Nam: ", "", data2.nam],
								["Bắc: ", "", data2.bac],
								["Đông-Bắc: ", "", data2.dongbac],
								["Đông-Nam: ", "", data2.dongnam],
								["Tây-Bắc: ", "", data2.taybac],
								["Tây-Nam: ", "", data2.taynam]
							];
							createTableRows(rows);
						}
						//bang.empty();
						//$("#xemve").val("0");
						//$("#xemve option:selected").text("Hướng phong thủy");
					});
				}
			}
			//khi nam có chọn và nữ không chọn
			else if(ttNam !="-1" && ttNu =="-1")
			{
				//khi năm sinh âm lớn hơn năm xem
				if(nam[ttNam].namam > namxem )
				{
					alert("Năm xem đang nhỏ hơn Năm sinh. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Hướng phong thủy");
				}
				//khi khoảng cách giữa năm sinh âm và năm xem lớn hơn 120 năm
				else if((namxem-nam[ttNam].namam +1) >120)
				{
					alert("Khoảng cách giữa Năm xem và Năm sinh lớn hơn 120 năm. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Hướng phong thủy");
				}
				else
				{
					$("#tieudeXemVe").text("");
					$("#tieudeNu").text("");
					$("#tieudeNam").text("NAM");
					var rows=[];

					$.post("/batdauxem/layHuongPhongThuy",{
					cungPhi: CUNGPHINAM[nam[ttNam].namam % 9] 
					} 
					,function(data2){

						if(data2=="0")
						{
							$("#warning").show();
							rows = [
								["", "", ""]
							];

							createTableRows(rows);
						}
						else
						{
							rows = [
							["", "<b>" +CUNGPHINAM[nam[ttNam].namam % 9] + "</b><br>" + " (" + MENHPHONGTHUYNAM[(nam[ttNam].namam%9)] + ")", ""],
							["Đông: ", data2.dong, ""],
							["Tây: ",data2.tay, ""],
							["Nam: ",data2.nam, ""],
							["Bắc: ",data2.bac, ""],
							["Đông-Bắc: ",data2.dongbac, ""],
							["Đông-Nam: ",data2.dongnam, ""],
							["Tây-Bắc: ",data2.taybac, ""],
							["Tây-Nam: ",data2.taynam, ""]
						];

						//bang.empty();
						createTableRows(rows);
						//$("#xemve").val("0");
						//$("#xemve option:selected").text("Hướng phong thủy");
						}	
					});
				}
			}
			else if(ttNam !="-1" && ttNu !="-1")
			{
				//khi năm sinh âm lớn hơn năm xem
				if(nam[ttNam].namam > namxem || nu[ttNu].namam > namxem)
				{
					alert("Năm xem đang nhỏ hơn Năm sinh. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Hướng phong thủy");
				}
				//khi khoảng cách giữa năm sinh âm và năm xem lớn hơn 120 năm
				else if((namxem-nam[ttNam].namam +1) >120 || (namxem-nu[ttNu].namam +1) >120)
				{
					alert("Khoảng cách giữa Năm xem và Năm sinh lớn hơn 120 năm. Vui lòng chọn lại Năm xem!");
					//$("#xemve").val("0");
					//$("#xemve option:selected").text("Hướng phong thủy");
				}
				else
				{
					$("#tieudeXemVe").text("");
					$("#tieudeNu").text("NỮ");
					$("#tieudeNam").text("NAM");

					var rows=[];

					$.post("/batdauxem/layHuongPhongThuy",{
					cungPhi: CUNGPHINAM[nam[ttNam].namam % 9] 
					} 
					,function(data2){
						$.post("/batdauxem/layHuongPhongThuy",{
						cungPhi: CUNGPHINU[nu[ttNu].namam % 9] 
						} 
						,function(data){
							if(data2=="0")
							{
								$("#warning").show();
								rows = [
									["", "", ""]
								];

								createTableRows(rows);
							}
							else
							{
								rows = [
								["", "<b>" + CUNGPHINAM[nam[ttNam].namam % 9] + "</b><br>" + " (" + MENHPHONGTHUYNAM[(nam[ttNam].namam%9)] + ")", "<b>" +CUNGPHINU[nu[ttNu].namam % 9] + "</b><br>" + " (" + MENHPHONGTHUYNU[(nu[ttNu].namam%9)] + ")"],
								["Đông: ", data2.dong, data.dong],
								["Tây: ", data2.tay, data.tay],
								["Nam: ", data2.nam, data.nam],
								["Bắc: ", data2.bac, data.bac],
								["Đông-Bắc: ", data2.dongbac, data.dongbac],
								["Đông-Nam: ", data2.dongnam, data.dongnam],
								["Tây-Bắc: ", data2.taybac, data.taybac],
								["Tây-Nam: ", data2.taynam, data.taynam]
							];

							//bang.empty();
							createTableRows(rows);
							//$("#xemve").val("0");
							//$("#xemve option:selected").text("Hướng phong thủy");
							}	
						});
					});
				}
			}
			else
			{
				alert("Vui lòng chọn 1 đối tượng để xem");
				//$("#xemve").val("0");
				//$("#xemve option:selected").text("Hướng phong thủy");
			}
		}
	}

	// khi xem về thay đổi
	$("#xemve").change(function(){
		var luachon= $("#xemve").val();
		showAllInfo(nam, nu, namxem, luachon);
	});

	$("#btnGui").click(function(){
		var sdt= $("#sdtGui").val();
		var noiDung = $("#content").val();

		$.post('/users/guiThongTin',
		{
			sdt: sdt,
			content:noiDung
		},function(data){
			if(data=="Gửi thành công"){
				$("#sdtGui").val("");
				$("#content").val("");
				//$('#exampleModal').modal('hide');
			}
			alert(data);
		});
	});
});