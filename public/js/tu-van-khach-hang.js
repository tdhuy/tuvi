$(document).ready(function(){

	function lienhe(){
		var bang =$('#noiDungBang');
		bang.empty();

		$.post('/admin/layThongTinLienHe',function(data){
			if(data!=null)
			{	
				data.forEach(function(val){
					var Element=jQuery("<tr></tr>");
					var createElement1=jQuery('<td></td>');

					var eleA  = jQuery('<a style=" color:gold">' + val.sdt + "</a>");
					eleA.attr("href", "tel:" + val.sdt);
					createElement1.append(eleA);

					var createElement2=jQuery('<td>' + val.ngaygui + "</td>");
					var createElement3=jQuery('<td>' + val.noidung + "</td>");
					var createElement4=jQuery('<td></td>');
					var createdButton = jQuery('<button>Xác nhận</button>');
					createdButton.addClass("btnXacNhan");
					createdButton.attr("value", val.id);
					createdButton.attr("name", "btnId");
					createdButton.attr("type", "button");

					createdButton.click(function(){
						$.post("/admin/daLienHe",
						{
							id: val.id
						},function(data2)
						{
							lienhe();
						});
					});

					//createdButton.attr("onclick", )

					createElement4.append(createdButton);
					Element.append(createElement1);
					Element.append(createElement2);
					Element.append(createElement3);
					Element.append(createElement4);

					bang.append(Element);
				});
			}
		});
	}

	lienhe();
});