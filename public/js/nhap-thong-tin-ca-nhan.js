$(document).ready(function(){
	var ten, gioiTinh, gio ,ngayDuong, thangDuong, namDuong, ngayAm, thangAm, namAm, giosinh, canAm, chiAm, conThu,
	 namSinhCha, namSinhMe, namKetHon, namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon 
	 ,Element, i, n, gioHoangDao;

	 //timThangDuThangThieu(1, 5, 2019);

	 namAm=0;
	 ngayDuong=0;
	 thangDuong=0;
	 namDuong=0;
	 ngayAm=0;
	 thangAm=0;

	 i=1900;
	 n=2080;
	 Element=$(".year1");

	 var Element1=$("#conthu");

	//khởi tao ngày tháng nam sinh
	$(function() {

		var date = new Date();

		$(".form_datetime").dateDropdowns({
			submitFieldName: 'form_datetime',
			submitFormat: "dd/mm/yyyy",
			//defaultDate: parseStandard(date.getDate())+"/"+parseStandard(date.getMonth()+1)+"/"+parseStandard(date.getFullYear())
		});

		//printLunarYear();
	});
	

	for(i;i<n;i++)
	{
		if(i==1900)
		{
			var createElement=jQuery('<option>'+ "----" +'</option>');
			createElement.attr("value", 0);
			Element.append(createElement);
			var newElement= jQuery('<option>'+ i +'</option>');
			newElement.attr("value", i)
			Element.append(newElement);
		}
		var newElement= jQuery('<option>'+ i +'</option>');
		newElement.attr("value", i)
		Element.append(newElement);
	}

	//khởi tạo opption cho con thứ
	for(var j=2;j<16;j++)
	{
		var newElement= jQuery('<option>'+ j +'</option>');
		newElement.attr("value", j)
		Element1.append(newElement);
	}

	//khởi tạo opption cho con thứ bên vợ
	var conThuDoiPhuong=$("#conthuvo");
	for(var j=2;j<16;j++)
	{
		var newElement= jQuery('<option>'+ j +'</option>');
		newElement.attr("value", j)
		conThuDoiPhuong.append(newElement);
	}

	//khởi tạo opption cho con thứ bên chồng
	var conThuDoiPhuong=$("#conthuchong");
	for(var j=2;j<16;j++)
	{
		var newElement= jQuery('<option>'+ j +'</option>');
		newElement.attr("value", j)
		conThuDoiPhuong.append(newElement);
	}

	//khởi tạo option cho số con bên nam
	var Element2=$("#socon");
	for(var j=0;j<14;j++)
	{
		var newElement= jQuery('<option>'+ j +'</option>');
		newElement.attr("value", j)
		Element2.append(newElement);
	}

	//khởi tạo option cho số con bên nam
	var element2=$("#socon1");
	for(var j=0;j<14;j++)
	{
		var newElement= jQuery('<option>'+ j +'</option>');
		newElement.attr("value", j)
		element2.append(newElement);
	}

	//chuẩn hóa chuỗi
	function parseStandard(x){
		if(Number(x) < 10)
			x = "0" + x;
		return x;  
	}

	//khỏi tạo năm sinh mặc định
	// function ramdomYearBorn(year, elment1, elment2)
	// {
	// 	if(year > 1926 )
	// 	{
	// 		elment1.val(Number(year)-25);
	// 		elment2.val(Number(year)-25);
	// 	}
	// 	else{
	// 		elment1.val("1900");
	// 		elment2.val("1900");
	// 	}
	// }

	function getTime(month)
	{
		$.post("/ban-than/gettime", {
		    month: month
		  } 
		  ,function(data2){
			
			var element=$("#giosinh");
			element.attr("disabled", false);
			element.empty();
			//gioHoangDao=data2;

			data2.forEach(function(val, item) {
				if(item==0)
				{
					var createdElement = jQuery('<option>'+ "------------------" +'</option>');
					createdElement.attr("value", "");
					element.append(createdElement);
					var newElement= jQuery('<option>'+ data2[item].khunggio +'</option>');
					newElement.attr("value", data2[item].id);
					element.append(newElement);
				}
				else
				{
					var newElement= jQuery('<option>'+ data2[item].khunggio +'</option>');
					newElement.attr("value", data2[item].id);
					element.append(newElement);	
				}
			});
		});
	};

	//hàm tạo giờ và năm âm
	function printLunarYear()
	{
		var namsinh=$(".form_datetime").val();
		$("#AL").text("");
		var element=$("#giosinh");
		element.attr("disabled", true);
		element.empty();

		if(namsinh=="")
		{
			getDate();
		}
		else{
			var lunarDay, arr, jd, canChi;
			var namsinh=$(".form_datetime").val();

			//$("#lich").attr("disabled", false);
			arr=namsinh.split("/");
			//$(".form_datetime").val("");

			//ramdomYearBorn(arr[2], $("#namsinhcha") , $("#namsinhme"));

			test=convertSolar2Lunar(Number(arr[0]), Number(arr[1]), Number(arr[2]), 7.0);

			jd=jdn(Number(arr[0]), Number(arr[1]), Number(arr[2]));
			canChi=getCanChi(test[0],test[1],test[2],test[3], jd);

			$("#AL").text(parseStandard(test[0])+ "/" + parseStandard(test[1]) + "/" + parseStandard(test[2])+" ("+ canChi[0] 
			+"/"+ canChi[1]+ "/"+canChi[2]+")");
			
			var canChiNamAm=getYearCanChi1(Number(test[2]));

			ngayDuong=Number(arr[0]);
			thangDuong=Number(arr[1]);
			namDuong=Number(arr[2]);
			ngayAm=test[0];
			thangAm=test[1];
			namAm=test[2];
			canAm=canChiNamAm[0];
			chiAm=canChiNamAm[1];

			getTime(test[1]);
		}	
	}

	//đổi lịch âm sang dương
	function printSolarYear(){
		var namsinh=$(".form_datetime").val();
		$("#AL").text("");
		var element=$("#giosinh");
		element.attr("disabled", true);
		element.empty();

		if(namsinh=="")
		{
			getDate();
		}
		else{
			var lunarDay, arr, jd, canChi;

			arr=namsinh.split("/");
			//$(".form_datetime").val("");	

			test=convertLunar2Solar(Number(arr[0]), Number(arr[1]), Number(arr[2]), 0, 7);		

			jd=jdn(test[0], test[1], test[2]);
			canChi=getCanChi(Number(arr[0]),Number(arr[1]),Number(arr[2]),0, jd);

			$("#AL").text(parseStandard(Number(arr[0]))+ "/" + parseStandard(Number(arr[1])) + "/" + parseStandard(Number(arr[2]))+" ("+ canChi[0] 
			+"/"+ canChi[1]+ "/"+canChi[2]+")");
			
			var canChiNamAm=getYearCanChi1(Number(arr[2]));

			ngayDuong=test[0];
			thangDuong=test[1];
			namDuong=test[2];
			ngayAm=Number(arr[0]);
			thangAm=Number(arr[1]);
			namAm=Number(arr[2]);
			canAm=canChiNamAm[0];
			chiAm=canChiNamAm[1];

			//$(".form_datetime").val(parseStandard(test[0])+"/"+parseStandard(test[1])+"/"+parseStandard(test[2]));

			//$("#lich").val("1");

			getTime(Number(arr[1]));
		}
	};

	function getDate()
	{
		var lich = $("#lich").val();
		if(lich=="1")
		{
			if($(".day").val()!="")
			{
				ngayDuong=$(".day").val();

				if($(".month").val()!="")
				{
					thangDuong=$(".month").val();
				}
				else
				{
					thangDuong = 0;
				}
				if($(".year").val()!="")
				{
					namDuong=$(".year").val();
					namAm=$(".year").val();

					var canChiNamAm=getYearCanChi1(Number($(".year").val()));
					canAm=canChiNamAm[0];
					chiAm=canChiNamAm[1];
				}
				else
				{
					namDuong=0;
					namAm=0;
				}
			}
			else
			{
				ngayDuong=0;
			}

			if($(".month").val()!="")
			{
				thangDuong=$(".month").val();

				if($(".day").val()!="")
				{
					ngayDuong=$(".day").val();
				}
				else
				{
					ngayDuong=0;
				}

				if($(".year").val()!="")
				{
					namDuong=$(".year").val();
					namAm=$(".year").val();
					//alert(namDuong);
					var canChiNamAm=getYearCanChi1(Number($(".year").val()));
					canAm=canChiNamAm[0];
					chiAm=canChiNamAm[1];
				}
				else
				{
					namDuong=0;
					namAm=0;
				}
			}
			else
			{
				thangDuong=0;
			}

			if($(".year").val()!="")
			{
				namDuong=$(".year").val();
				namAm=$(".year").val();

				var canChiNamAm=getYearCanChi1(Number($(".year").val()));

				canAm=canChiNamAm[0];
				chiAm=canChiNamAm[1];

				if($(".day").val()!="")
				{
					ngayDuong=$(".day").val();
				}
				else
				{
					ngayDuong=0;
				}

				if($(".month").val()!="")
				{
					thangDuong=$(".month").val();
				}
				else
				{
					thangDuong = 0;
				}
			}
			else
			{
				namDuong=0;
				namAm=0;
			}			
		}
		else
		{
			if($(".day").val()!="")
			{
				ngayAm=$(".day").val();

				if($(".month").val()!="")
				{
					thangAm=$(".month").val();
					getTime(Number(thangAm));
				}
				else
				{
					var element=$("#giosinh");
					element.attr("disabled", true);
					element.empty();
					thangAm = 0;
				}
				if($(".year").val()!="")
				{
					namAm=$(".year").val();
					namDuong=$(".year").val();

					var canChiNamAm=getYearCanChi1(Number($(".year").val()));
					canAm=canChiNamAm[0];
					chiAm=canChiNamAm[1];
				}
				else
				{
					namDuong=0;
					namAm=0;
				}
			}
			else
			{
				ngayAm=0;
			}

			if($(".month").val()!="")
			{
				thangAm=$(".month").val();
				getTime(Number(thangAm));

				if($(".day").val()!="")
				{
					ngayAm=$(".day").val();
				}
				else
				{
					ngayAm=0;
				}

				if($(".year").val()!="")
				{
					namDuong=$(".year").val();
					namAm=$(".year").val();

					var canChiNamAm=getYearCanChi1(Number($(".year").val()));
					canAm=canChiNamAm[0];
					chiAm=canChiNamAm[1];
				}
				else
				{
					namDuong=0;
					namAm=0;
				}
			}
			else
			{
				thangAm=0;
				var element=$("#giosinh");
				element.attr("disabled", true);
				element.empty();
			}

			if($(".year").val()!="")
			{
				namDuong=$(".year").val();
				namAm=$(".year").val();

				var canChiNamAm=getYearCanChi1(Number($(".year").val()));

				canAm=canChiNamAm[0];
				chiAm=canChiNamAm[1];

				if($(".day").val()!="")
				{
					ngayAm=$(".day").val();
				}
				else
				{
					ngayAm=0;
				}

				if($(".month").val()!="")
				{
					thangAm=$(".month").val();
					getTime(Number(thangAm));
				}
				else
				{
					thangAm = 0;
					var element=$("#giosinh");
					element.attr("disabled", true);
					element.empty();
				}
			}
			else
			{
				namDuong=0;
				namAm=0;
			}	
		}
	}
				
	//bắt sự kiện khi nhập ngày tháng năm
	$(".form_datetime").change(function() {

		var lich = $("#lich").val();
		var namsinh=$(".form_datetime").val();
		ngayDuong = 0;
	    thangDuong = 0;
	    namDuong = 0;
	    ngayAm = 0;
	    thangAm = 0;
	    namAm = 0;
	    canAm = "";
	    chiAm = "";

		if(namsinh=="")
		{
			$("#AL").text("");
			var element=$("#giosinh");
			element.attr("disabled", true);
			element.empty();

			getDate();
		}
		else
		{
			//alert($(".year").val());
			if(lich=="1")
			{
				printLunarYear();
			}
			else
			{
				printSolarYear();
			}
		}
	});

	$("#lich").change(function(){
		var lich = $("#lich").val();
		ngayDuong = 0;
	    thangDuong = 0;
	    namDuong = 0;
	    ngayAm = 0;
	    thangAm = 0;
	    namAm = 0;
	    canAm = "";
	    chiAm = "";

		if(lich=="1")
		{
			printLunarYear();
		}
		else
		{
			printSolarYear();
		}
	});

	// function getYearForChildren(element, y)
	// {
	// 	if(Number(y)+25 > 2079)
	// 	{
	// 		element.val("2079");
	// 	}
	// 	else
	// 	{
	// 		element.val((Number(y)+25));
	// 	}
	// }

	//tạo select giới tính
	function createOptionSex(e)
	{
		var createElement=jQuery('<option>Nam</option>');
		createElement.attr("value", 1);
		e.append(createElement);
		var createElement1=jQuery('<option>Nữ</option>');
		createElement1.attr("value", 2);
		e.append(createElement1);
	}

	//tạo select năm sinh của con
	function showChildren(element, n)
	{
		if(n!=0)
		{
			element.show();
			for(var j=0; j<n;j++)
			{
				var ele1=jQuery("<div> </div>");
				ele1.addClass("row no-gutter");

				var ele2=jQuery("<div>" +"Năm sinh con thứ "+ (j+1) +"</div>");
				ele2.addClass("col-sm-6");

				var ele3=jQuery("<div> </div>");
				ele3.addClass("col-sm-6");

				var ele4=jQuery("<div> </div>");
				ele4.addClass("row");

				var ele5=jQuery("<div> </div>");
				ele5.addClass("col-6");
				ele5.css("padding-right", "5px");

				var ele6=jQuery("<select> <select>");
				ele6.attr("id", "conthu"+ (j+1));
				ele6.addClass("namsinhcuacon");

				var ele7=jQuery("<div> </div>");
				ele7.addClass("col-6");
				ele7.css("padding-left", "0px");

				var ele8=jQuery("<select> <select>");
				ele8.attr("id", "gioitinhconthu"+ (j+1));
				ele8.addClass("gioitinhcuacon");


				ele5.append(ele6);
				ele7.append(ele8);

				ele4.append(ele5);
				ele4.append(ele7);

				ele3.append(ele4);

				ele1.append(ele2);
				ele1.append(ele3);


				element.append(ele1);
			}

			var namsinhcuacon=$(".namsinhcuacon");
			var gioitinhcuacon=$(".gioitinhcuacon");
			for(i=1900;i<2080;i++)
			{
				if(i==1900)
				{
					var createElement=jQuery('<option>'+ "----" +'</option>');
					createElement.attr("value", 0);
					//createElement.attr("selected", "selected");
					namsinhcuacon.append(createElement);
					var newElement= jQuery('<option>'+ i +'</option>');
					newElement.attr("value", i)
					namsinhcuacon.append(newElement);
				}
				var newElement= jQuery('<option>'+ i +'</option>');
				newElement.attr("value", i)
				namsinhcuacon.append(newElement);
			}

			
			//getYearForChildren(namsinhcuacon, namAm);
			createOptionSex(gioitinhcuacon);

		}
		else
		{
			element.hide();
		}
	}

	var con_cai=$("#con-cai");
	//khi số con  thay đổi bên nam
	Element2.change(function(){
		var soLuong=Number(Element2.val());
		con_cai.empty();

		showChildren(con_cai, soLuong);
	});

	//khi con cái thay đổi bên nữ
	element2.change(function(){
		var soLuong=Number(element2.val());
		con_cai.empty();

		showChildren(con_cai, soLuong);
		// if(soLuong!=0)
		// {
		// 	con_cai.show();
		// 	for(var j=0; j<soLuong;j++)
		// 	{
		// 		var Element7=jQuery("<div> </div>");
		// 		Element7.addClass("row no-gutter");

		// 		var Element4=jQuery("<div>" +"Năm sinh con thứ "+ (j+1) +"</div>");
		// 		Element4.addClass("col-6");

		// 		var Element5=jQuery("<div> </div>");
		// 		Element5.addClass("col-6");

		// 		var Element6=jQuery("<select> <select>");
		// 		Element6.attr("id", "conthu"+ (j+1));
		// 		Element6.addClass("namsinhcuacon");

		// 		Element5.append(Element6);
		// 		Element7.append(Element4);
		// 		Element7.append(Element5);
		// 		con_cai.append(Element7);
		// 	}

		// 	var namsinhcuacon=$(".namsinhcuacon");
		// 	for(i=1900;i<n;i++)
		// 	{
		// 		if(i==1900)
		// 		{
		// 			var createElement=jQuery('<option>'+ "----" +'</option>');
		// 			createElement.attr("value", 0);
		// 			namsinhcuacon.append(createElement);
		// 			var newElement= jQuery('<option>'+ i +'</option>');
		// 			newElement.attr("value", i)
		// 			namsinhcuacon.append(newElement);
		// 		}
		// 		var newElement= jQuery('<option>'+ i +'</option>');
		// 		newElement.attr("value", i)
		// 		namsinhcuacon.append(newElement);
		// 	}
		// }
		// else
		// {
		// 	con_cai.hide();
		// }
	});


	//khi năm sinh vợ thay đổi
	// $('#namsinhvo').change(function(){
	// 	ramdomYearBorn($('#namsinhvo').val(), $('#namsinhchavo'), $('#namsinhmevo'));
	// });

	//khi năm sinh chồng thay đổi
	// $('#namsinhchong').change(function(){
	// 	ramdomYearBorn($('#namsinhchong').val(), $('#namsinhchachong'), $('#namsinhmechong'));
	// });
	
	function change()
	{
		$("#hon-nhan-nu").hide();
		$("#hon-nhan-nam").hide();
		con_cai.hide();
		$("#socon1").val("0");
		$("#socon").val("0");

		if($("#gioitinh").val()=='1' && $("#kethon").val()!="0")
		{
			$("#hon-nhan-nam").show();
		}
		else if($("#gioitinh").val()=='2' && $("#kethon").val()!="0")
		{
			$("#hon-nhan-nu").show();	
		}
		else
		{
			$("#hon-nhan-nu").hide();
			$("#hon-nhan-nam").hide();
		}
	}
	
	//khi kết hôn thay đổi
	$("#kethon").change(function(){
		change();
	});

	//khi giới tinh thay đổi
	$("#gioitinh").change(function(){
		change();
	});

	// $(".year").change(function()
	// {
	// 	if($(".year").val()!='0')
	// 	{
	// 		alert("Thay đổi nè");
	// 		$("#namsinhcha").val($(".year").val()-25);
	// 		$("#namsinhme").val($(".year").val()-25);
	// 	}
	// });	

	

	//đưa trở  ngày hiện tại
	// function getDatetoDay( callback) {
	// 	var date = new Date();
	//   	$(".day").val(parseStandard(date.getDate()));
	// 	$(".month").val(parseStandard(date.getMonth()+1));
	// 	$(".year").val(parseStandard(date.getFullYear()));
	// 	$(".form_datetime").val(parseStandard(date.getDate())+"/"+parseStandard(date.getMonth()+1)+"/"+parseStandard(date.getFullYear()));
	//   	callback();
	// }

	function XoaThongTin() {
		$("#hoten").val("");
		$("#gioitinh").val("1");
		$(".day").val("");
		$(".month").val("");
		$(".year").val("");
		$("#AL").text("");
		$("#giosinh").empty();
		$("#lich").val("1");
		$("#namsinhcha").val("0");
		$("#namsinhme").val("0");
		$("#conthu").val("2");
		$("#conthuvo").val("2");
		$("#conthuchong").val("2");
		$("#kethon").val("0");
		$("#namsinhchong").val("0");
		$("#namsinhchachong").val("0");
		$("#namsinhmechong").val("0");
		$("#namsinhvo").val("0");
		$("#namsinhchavo").val("0");
		$("#namsinhmevo").val("0");
		$("#socon1").val("0");
		$("#socon").val("0");
		$("#hon-nhan-nu").hide();
		$("#hon-nhan-nam").hide();
		ngayDuong = 0;
	    thangDuong = 0;
	    namDuong = 0;
	    ngayAm = 0;
	    thangAm = 0;
	    namAm = 0;
	    canAm = "";
	    chiAm = "";

		con_cai.hide();
	}

//tạo hàm luu thong tin khach hàng
	function luuThongTin(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong, ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe, namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon, namKetHon, conThuDP, gioiTinhCacCon){
		$.post("/ban-than/saveInfo", {
	    	ten,
	    	gioiTinh,
	    	gio,
	    	ngayDuong,
	    	thangDuong,
	    	namDuong,
	    	ngayAm,
	    	thangAm,
	    	namAm,
	    	canAm,
	    	chiAm,
	    	conThu,
	    	namSinhCha,
	    	namSinhMe,
	    	namSinhDoiPhuong,
	    	namSinhChaDoiPhuong,
	    	namSinhMeDoiPhuong,
	    	soCon,
	    	namSinhCacCon,
	    	namKetHon,
	    	conThuDP,
	    	gioiTinhCacCon
		  } 
		  ,function(data2){
			alert(data2);
			XoaThongTin();
		});	
	}

	//lưu thông tin khach hàng

	$("#btnThemNguoiXem").click(function(){
		gt=["","Nam", "Nữ"];

		gio="";
		ten=$("#hoten").val();
		gioiTinh=gt[$("#gioitinh").val()];
		conThu=$("#conthu").val();
		namSinhCha=$("#namsinhcha").val();
		namSinhMe=$("#namsinhme").val();
		namKetHon=0;
		namSinhDoiPhuong=0;
		namSinhChaDoiPhuong=0;
		namSinhMeDoiPhuong=0;
		soCon=0;
		namSinhCacCon="";
		var conThuDP=0;
		var gioiTinhCacCon="";

		if(namAm==0)
		{
			alert("Vui lòng chọn năm");
		}
		else
		{
			if($("#kethon").val()=="0")
			{ 	
				if(thangAm==0||$("#giosinh").val()=="")
				{
					gio="";
				}
				else
				{
					gio=$("#giosinh").val();
				}

				if(ten=="")
				{
					ten="Khách";
				}
				
				// alert("Tên: "+ten+"\n.Giới tính: "+gioiTinh+"\n.Giờ: " +gio+ "\n.Ngày dương: "+ ngayDuong + "\n.Tháng dương: " +thangDuong + "\n.Năm dương: " +namDuong +"\n. Ngày âm: "+ngayAm
				// +"\n.Tháng âm: "+thangAm+"\n.Năm âm: "+namAm+"\n. Can âm: " +canAm+ "\n.chi âm: "+ chiAm +"\n.con thứ: "+conThu+ "\n.năm sinh cha: "+ namSinhCha+"\n.năm sinh mẹ: "+ namSinhMe);

				luuThongTin(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
				 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
				  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
				   namKetHon, conThuDP, gioiTinhCacCon);
			}
			else 
			{
				if(gioiTinh=="Nữ")
				{
					namKetHon=$("#kethon").val();
					namSinhDoiPhuong=$("#namsinhchong").val();
					namSinhChaDoiPhuong=$("#namsinhchachong").val();
					namSinhMeDoiPhuong=$("#namsinhmechong").val();
					soCon=$("#socon1").val();
					conThuDP=$("#conthuchong").val();
					namSinhCacCon="";
					
					if(soCon=="0")
					{
						if(thangAm==0||$("#giosinh").val()=="")
						{
							gio="";
						}
						else
						{				
							gio=$("#giosinh").val();
						}

						if(ten=="")
						{
							ten="Khách";
						}

						luuThongTin(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
							 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
							  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
							   namKetHon, conThuDP, gioiTinhCacCon);
					}
					else
					{
						if(thangAm==0||$("#giosinh").val()=="")
						{
							gio="";
						}
						else
						{							
							gio=$("#giosinh").val();
						}

						if(ten=="")
						{
							ten="Khách";
						}

						for(var j=0; j<soCon;j++)
						{
							var con=$("#conthu"+ (j+1)).val();
							namSinhCacCon+=con+"-";
						}

						for(var j=0; j<soCon;j++)
						{
							var con=$("#gioitinhconthu"+ (j+1)).val();
							gioiTinhCacCon+=gt[con]+"-";
						}

						namSinhCacCon=namSinhCacCon.slice(0, -1);
						gioiTinhCacCon=gioiTinhCacCon.slice(0, -1);

						luuThongTin(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
							 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
							  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
							   namKetHon, conThuDP, gioiTinhCacCon);
					}
				}			
				else
				{
					namKetHon=$("#kethon").val();
					namSinhDoiPhuong=$("#namsinhvo").val();
					namSinhChaDoiPhuong=$("#namsinhchavo").val();
					namSinhMeDoiPhuong=$("#namsinhmevo").val();
					soCon=$("#socon").val();
					conThuDP=$("#conthuvo").val();
					namSinhCacCon="";

					if(soCon=="0")
					{
						if(thangAm==0||$("#giosinh").val()=="")
						{
							gio="";
						}
						else
						{
							gio=$("#giosinh").val();
						}

						if(ten=="")
						{
							ten="Khách";
						}

						luuThongTin(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
							 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
							  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
							   namKetHon, conThuDP, gioiTinhCacCon);
					}
					else
					{
						if(thangAm==0||$("#giosinh").val()=="")
						{
							gio="";
						}
						else
						{			
							gio=$("#giosinh").val();
						}

						if(ten=="")
						{
							ten="Khách";
						}

						for(var j=0; j<soCon;j++)
						{
							var con=$("#conthu"+ (j+1)).val();
							namSinhCacCon+=con+"-";
						}

						for(var j=0; j<soCon;j++)
						{
							var con=$("#gioitinhconthu"+ (j+1)).val();
							gioiTinhCacCon+=gt[con]+"-";
						}

						namSinhCacCon=namSinhCacCon.slice(0, -1);
						gioiTinhCacCon=gioiTinhCacCon.slice(0, -1);
						// alert(namSinhCacCon);
						// alert(gioiTinhCacCon);
						luuThongTin(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
							 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
							  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
							   namKetHon, conThuDP, gioiTinhCacCon);
		
					}
				}
			}
		}	
	});

	//tạo obj 
	function createObj(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong, ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe, namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon, namKetHon, conThuDP, gioiTinhCacCon){
		var obj = {
	    	ten:ten,
	    	gioitinh:gioiTinh,
	    	giohoangdao:gio,
	    	ngayduong:ngayDuong,
	    	thangduong: thangDuong,
	    	namduong: namDuong,
	    	ngayam:ngayAm,
	    	thangam:thangAm,
	    	namam:namAm,
	    	canam:canAm,
	    	chiam:chiAm,
	    	conthu:conThu,
	    	namsinhcha: namSinhCha,
	    	namsinhme:namSinhMe,
	    	namsinhdoiphuong:namSinhDoiPhuong,
	    	namsinhchadoiphuong:namSinhChaDoiPhuong,
	    	namsinhmedoiphuong:namSinhMeDoiPhuong,
	    	socon:soCon,
	    	namsinhcaccon:namSinhCacCon,
	    	namkethon:namKetHon,
	    	conthudp:conThuDP,
	    	gioitinhcaccon:gioiTinhCacCon
		 };
		 obj=JSON.stringify(obj);

		 //alert(obj);
		 return obj;
	}

	//khi bắt đầu xem click
	$("#btnBatDauXem").click(function(){
		gt=["","Nam", "Nữ"];

		gio="";
		ten=$("#hoten").val();
		gioiTinh=gt[$("#gioitinh").val()];
		conThu=$("#conthu").val();
		namSinhCha=$("#namsinhcha").val();
		namSinhMe=$("#namsinhme").val();
		namKetHon=0;
		namSinhDoiPhuong=0;
		namSinhChaDoiPhuong=0;
		namSinhMeDoiPhuong=0;
		soCon=0;
		namSinhCacCon="";
		var conThuDP=0;
		var gioiTinhCacCon="";

		if(namSinhCha != 0 && namAm==0 || namSinhMe !=0 && namAm==0 || ngayDuong != 0 && namAm==0 || ngayDuong != 0 && namAm==0 || ngayAm != 0 && namAm==0 || thangAm != 0 && namAm==0)
		{
			alert("Vui lòng chọn năm sinh");
			return false;
		}
		else
		{
			if($("#kethon").val()=="0")
			{ 
				if(thangAm==0||$("#giosinh").val()=="")
				{
					gio="";
				}
				else
				{			
					gio=$("#giosinh").val();
				}

				if(ten=="")
				{
					ten="Khách";
				}
				var obj = createObj(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
				 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
				  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
				   namKetHon, conThuDP, gioiTinhCacCon);

				$("#btnBatDauXem").val(obj);		
			}
			else 
			{
				if(gioiTinh =="Nữ")
				{
					namKetHon=$("#kethon").val();
					namSinhDoiPhuong=$("#namsinhchong").val();
					namSinhChaDoiPhuong=$("#namsinhchachong").val();
					namSinhMeDoiPhuong=$("#namsinhmechong").val();
					soCon=$("#socon1").val();
					conThuDP=$("#conthuchong").val();
					namSinhCacCon="";

		
					if(namKetHon!=0 && namAm==0)
					{
						alert("Vui lòng chọn năm sinh");
						return false;
					}
					else
					{
						if(soCon=="0")
						{
							if(thangAm==0||$("#giosinh").val()=="")
							{
								gio="";
							}
							else
							{			
								gio=$("#giosinh").val();
							}

							if(ten=="")
							{
								ten="Khách";
							}
							var obj = createObj(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
							 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
							  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
							   namKetHon, conThuDP, gioiTinhCacCon);

							$("#btnBatDauXem").val(obj);
						}
						else
						{
							if(thangAm==0||$("#giosinh").val()=="")
							{
								gio="";
							}
							else
							{			
								gio=$("#giosinh").val();
							}

							if(ten=="")
							{
								ten="Khách";
							}

							for(var j=0; j<soCon;j++)
							{
								var con=$("#conthu"+ (j+1)).val();
								namSinhCacCon+=con+"-";
							}
							for(var j=0; j<soCon;j++)
							{
								var con=$("#gioitinhconthu"+ (j+1)).val();
								gioiTinhCacCon+=gt[con]+"-";
							}

							namSinhCacCon=namSinhCacCon.slice(0, -1);
							gioiTinhCacCon=gioiTinhCacCon.slice(0, -1);

							var obj = createObj(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
							 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
							  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
							   namKetHon, conThuDP, gioiTinhCacCon);

							$("#btnBatDauXem").val(obj);
						}
					}
				}
				else
				{
					namKetHon=$("#kethon").val();
					namSinhDoiPhuong=$("#namsinhvo").val();
					namSinhChaDoiPhuong=$("#namsinhchavo").val();
					namSinhMeDoiPhuong=$("#namsinhmevo").val();
					soCon=$("#socon").val();
					conThuDP=$("#conthuvo").val();
					namSinhCacCon="";
	
					if(namKetHon!=0 && namAm == 0)
					{
						alert("Vui lòng chọn năm sinh");
						return false;
					}
					else
					{
						if(soCon=="0")
						{
							if(thangAm==0||$("#giosinh").val()=="")
							{
								gio="";
							}
							else
							{			
								gio=$("#giosinh").val();
							}

							if(ten=="")
							{
								ten="Khách";
							}
							var obj = createObj(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
							 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
							  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
							   namKetHon, conThuDP, gioiTinhCacCon);

							$("#btnBatDauXem").val(obj);
						}
						else
						{
							if(thangAm==0||$("#giosinh").val()=="")
							{
								gio="";
							}
							else
							{			
								gio=$("#giosinh").val();
							}

							if(ten=="")
							{
								ten="Khách";
							}

							for(var j=0; j<soCon;j++)
							{
								var con=$("#conthu"+ (j+1)).val();
								namSinhCacCon+=con+"-";
							}

							for(var j=0; j<soCon;j++)
							{
								var con=$("#gioitinhconthu"+ (j+1)).val();
								gioiTinhCacCon+=gt[con]+"-";
							}
							namSinhCacCon=namSinhCacCon.slice(0, -1);
							gioiTinhCacCon=gioiTinhCacCon.slice(0, -1);

							var obj = createObj(ten, gioiTinh, gio, ngayDuong, thangDuong, namDuong,
							 ngayAm, thangAm, namAm, canAm, chiAm, conThu, namSinhCha, namSinhMe,
							  namSinhDoiPhuong, namSinhChaDoiPhuong, namSinhMeDoiPhuong, soCon, namSinhCacCon,
							   namKetHon, conThuDP, gioiTinhCacCon);

							$("#btnBatDauXem").val(obj);	
						}
					}
				}
			}
		}		
	});
});