$(document).ready(function() {

	if($("#alert3").text()!="")
	{
		alert($("#alert3").text());
		$("#alert3").text("");
	}

	if($("#alert").text()!="")
	{	
		if($("#alert").text()=="Đăng ký thành công.")
		{
			alert("Đăng ký thành công.");
		}
		else
		{
			$('#dangky').modal('toggle');
			$('#dangky').modal('show');
			$('#dangky').modal('hide');
		}
	}

	if($("#alert1").text()!="")
	{	
		if($("#alert1").text()=="Đăng nhập thành công.")
		{
			alert("Đăng nhập thành công.");
		}
		else
		{
			$('#dangnhap').modal('toggle');
			$('#dangnhap').modal('show');
			$('#dangnhap').modal('hide');
		}	
	}

	if($("#alert2").text()!="")
	{	
		if($("#alert2").text()=="Đổi mật khẩu thành công.")
		{
			alert("Đổi mật khẩu thành công.");
		}
		else
		{
			$('#doimatkhau').modal('toggle');
			$('#doimatkhau').modal('show');
			$('#doimatkhau').modal('hide');
		}	
	}

	function loadData(ele)
	{
		ele.empty();

		$.post("/users/layDuLieu", function(data){
			if(data !=null)
			{
				dataUsers=data;

				var ele1=jQuery('<option>'+ "- Chọn dữ liệu -" +'</option>');
				ele1.attr("value", "-1");
				ele.append(ele1);
				if(data != null)
				{
					data.forEach(function(val, item){
						var ele2=jQuery('<option>'+ val.ten + " " + val.namam +'</option>');
						ele2.attr("value", val.id);
						ele.append(ele2);				
					});
				}
			}
		});
	};

	function loadRoleUser(ele)
	{
		ele.empty();

		$.post("/admin/layQuyenNguoiDung", function(data){
			if(data !=null)
			{
				//dataUsers=data;

				var ele1=jQuery('<option>'+ "- Chọn dữ liệu -" +'</option>');
				ele1.attr("value", "-1");
				ele.append(ele1);

				if(data!=null)
				{
					data.forEach(function(val, item){
						var ele2=jQuery('<option>'+ val.ten + " " + val.sdt +'</option>');
						ele2.attr("value", val.sdt);
						ele.append(ele2);				
					});
				}
			}		
		});
	};

	function loadUser(ele)
	{
		ele.empty();

		$.post("/admin/layDuLieu", function(data){
			if(data !=null)
			{
				//dataUsers=data;

				var ele1=jQuery('<option>'+ "- Chọn dữ liệu -" +'</option>');
				ele1.attr("value", "-1");
				ele.append(ele1);

				if(data!=null)
				{
					data.forEach(function(val, item){
						var ele2=jQuery('<option>'+ val.ten + " " + val.sdt +'</option>');
						ele2.attr("value", val.sdt);
						ele.append(ele2);				
					});
				}
			}		
		});
	};

	if($("#role").text()!="")
	{
		$('#tabDangKy').hide();
		$('#tabDangNhap').hide();
		$('#tabDangXuat').show();
		$("#tabDoiMatKhau").show();
		$("#tabKhachHang").show();
		$("#tabCapQuyen").hide();
		$("#tabKhoiPhuc").hide();
		$("#tabTuVan").hide();
		$("#vip-icon").hide();

		var ele = $("#chonKH");
		var dataUsers;

		loadData(ele);

		ele.change(function(){
			if(ele.val() != "-1")
			{
				$("#btnSuaDLKH").attr("disabled", false);
				$("#btnXoaKH").attr("disabled", false);
			}
			else
			{
				$("#btnSuaDLKH").attr("disabled", true);
				$("#btnXoaKH").attr("disabled", true);
			}
		});

		$("#btnXoaKH").click(function(){

			$.post("/users/xoaDuLieu",
			{
				id: ele.val()
			}, function(data){
				alert(data);
				loadData(ele);
				$("#btnXoaKH").attr("disabled", true);
				$("#btnSuaDLKH").attr("disabled", true);
				$("#btnXoaKH").attr("disabled", true);
			});
		});

		if($("#role").text() == "2")
		{
			$("#tabCapQuyen").show();
			$("#tabKhoiPhuc").show();
			$("#tabTuVan").show();
			$("#vip-icon").show();

			var quyen =$("#idCapQuyen");
			var khoiPhucMatKhau = $("#idKhoiPhuc");

			loadRoleUser(quyen);
			loadUser(khoiPhucMatKhau);

			quyen.change(function()
			{
				if(quyen.val()!=-1)
				{
					$("#btnCapQuyen").attr("disabled", false);
				}
				else
				{
					$("#btnCapQuyen").attr("disabled", true);
				}
			});

			khoiPhucMatKhau.change(function()
			{
				if(khoiPhucMatKhau.val()!=-1)
				{
					$("#btnKhoiPhuc").attr("disabled", false);
				}
				else
				{
					$("#btnKhoiPhuc").attr("disabled", true);
				}
			});

			$("#btnCapQuyen").click(function(){
				$.post("/admin/capNhatQuyen",
				{
					sdt: quyen.val()
				},function(data)
				{
					alert(data);
					loadRoleUser(quyen);
					$("#btnCapQuyen").attr("disabled", true);
				})
			});


			$("#btnKhoiPhuc").click(function(){
				$.post("/admin/khoiPhucMatKhau",
				{
					sdt: khoiPhucMatKhau.val()
				},function(data)
				{
					alert(data);
					loadUser(khoiPhucMatKhau);
					$("#btnKhoiPhuc").attr("disabled", true);
				})
			});
		}
	}
	else
	{
		$('#tabDangKy').show();
		$('#tabDangNhap').show();
		$('#tabDangXuat').hide();
		$("#tabDoiMatKhau").hide();
		$("#tabKhachHang").hide();
		$("#tabCapQuyen").hide();
		$("#tabKhoiPhuc").hide();
		$("#vip-icon").hide();
	}
});