var express = require('express');
var router = express.Router();
const UserCtrl = require('../controllers/user.controller');

/* GET users listing. */
router.get('/', UserCtrl.getIndexPage);
router.post('/suaThongTin', UserCtrl.postSuaThongTinPage);
router.get('/suaThongTin', UserCtrl.getSuaThongTinPage);
router.post('/layDuLieu', UserCtrl.getDataUsers);
router.post('/xoaDuLieu', UserCtrl.deleteUser);
router.get('/nhapThongTin', UserCtrl.getSaveInfoPage);
router.post('/nhapThongTin', UserCtrl.getSaveInfoPage);
router.post('/layUser', UserCtrl.getUser);
router.post('/luuUser', UserCtrl.saveUser);
router.post('/guiThongTin', UserCtrl.addContent);

module.exports = router;
