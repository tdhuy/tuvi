var express = require('express');
var router = express.Router();
const UserCtrl = require('../controllers/admin.controller');

/* GET users listing. */
router.get('/', UserCtrl.getIndexPage);
router.post('/', UserCtrl.getIndexPage);
router.post('/layDuLieu', UserCtrl.getDataUsers);
router.post('/layQuyenNguoiDung', UserCtrl.getRoleDataUsers);
router.post('/capNhatQuyen', UserCtrl.updateRoleUser);
router.post('/khoiPhucMatKhau', UserCtrl.forgotPassword);
router.get('/lienHe', UserCtrl.getLienHePage);
router.post('/layThongTinLienHe', UserCtrl.getContent);
router.post('/daLienHe', UserCtrl.updateContent);

module.exports = router;
