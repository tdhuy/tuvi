var express = require('express');
var router = express.Router();
const UserCtrl = require('../controllers/kinh-dich.controller');

/* GET users listing. */
router.get('/', UserCtrl.getKinhDichPage);
router.post('/', UserCtrl.postKinhDichPage);
router.post('/layDuLieu', UserCtrl.getUsers);
router.post('/layKinhDich', UserCtrl.getYiJing);

module.exports = router;