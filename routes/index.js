var express = require('express');
var router = express.Router();
const UserCtrl = require('../controllers/index.controller.js'); 


/* GET home page. */
router.get('/', UserCtrl.getIndexPage);

router.post('/', UserCtrl.getIndexPage);

router.post('/ban-than', UserCtrl.getBanThanPage);

router.get('/ban-than', UserCtrl.getBanThanPage);

router.get("/ban-than/tu-tru", UserCtrl.getTuTruPage);

router.post("/ban-than/tu-tru", UserCtrl.getTuTruPage);

router.post('/ban-than/gettime', UserCtrl.getTime);

router.post('/ban-than/saveInfo', UserCtrl.saveInfo);

router.post('/ban-than/XemBanthan', UserCtrl.postBatDauXemPage);

router.get('/ban-than/XemBanthan', UserCtrl.getBatDauXemPage);

router.post('/ban-than/getDetail', UserCtrl.getDetail);

router.post('/batdauxem/nam',UserCtrl.getMaleInfo);

router.post('/batdauxem/nu',UserCtrl.getFemaleInfo);

router.post('/batdauxem/layNienVan',UserCtrl.getPeriodOfYear);

router.post('/batdauxem/layThaiTue',UserCtrl.getTaiSui);

router.post('/batdauxem/layTamTai',UserCtrl.getThreeFatalYears);

router.post('/batdauxem/layKoCuoiHoi',UserCtrl.getNotMarried);

router.post('/batdauxem/layHuongPhongThuy',UserCtrl.getFengShuiDirection);

router.post('/batdauxem/layThoiThe',UserCtrl.getTimes);

router.post('/batdauxem/ketHopCungPhi',UserCtrl.getCombination);

router.post('/batdauxem/layDuLieu',UserCtrl.getSubmittedData);

router.post('/index/dangKy', UserCtrl.register);

router.get('/index/dangKy', UserCtrl.getIndexPage);

router.post('/index/dangNhap', UserCtrl.login);

router.get('/index/dangNhap', UserCtrl.getIndexPage);

router.post('/index/dangXuat', UserCtrl.logout);

router.get('/index/dangXuat', UserCtrl.getIndexPage);

router.post('/index/doiMatKhau', UserCtrl.changePassWord);

router.get('/index/doiMatKhau', UserCtrl.getIndexPage);

router.post('/batdauxem/tsds', UserCtrl.getWuXing);

module.exports = router;
  
