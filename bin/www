#!/usr/bin/env node

/**
 * Module dependencies.
 */

 const app = require('../app');
 const debug = require('debug')('tuvi:server');
 const http = require('http');
 const config = require('config');

/**
 * Get port from environment and store in Express.
 */
 const appConfig = config.get('app');
 const port = normalizePort(appConfig.port || '3000');
 app.set('port', port);

/**
 * Create HTTP server.
 */

 const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

 const connectDB = require('../db.js');
 connectDB((connectSuccessfully) => {

  if (connectSuccessfully) {
    server.listen(port, () => {
      console.log(`Server is listen on port ${port}`);
    });
    server.on('error', onError);
  } else {
    console.error('Cannot connect to postgres');
  }
 });

 // server.listen(port, () => {
 // 	console.log(`Server is listen on port ${port}`);
 // });

/**
 * Normalize a port into a number, string, or false.
 */

 function normalizePort(val) {
 	var port = parseInt(val, 10);

 	if (isNaN(port)) {
    // named pipe
    return val;
}

if (port >= 0) {
    // port number
    return port;
}

return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

 function onError(error) {
 	if (error.syscall !== 'listen') {
 		throw error;
 	}

 	var bind = typeof port === 'string'
 	? 'Pipe ' + port
 	: 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
  	case 'EACCES':
  	console.error(bind + ' requires elevated privileges');
  	process.exit(1);
  	break;
  	case 'EADDRINUSE':
  	console.error(bind + ' is already in use');
  	process.exit(1);
  	break;
  	default:
  	throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

 function onListening() {
 	console.log('Server is running on port', port);
 	var addr = server.address();
 	var bind = typeof addr === 'string'
 	? 'pipe ' + addr
 	: 'port ' + addr.port;
 	debug('Listening on ' + bind);
 }
